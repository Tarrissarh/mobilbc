<?php

namespace app\components;

use Yii;
use yii\web\UrlRule;
use app\models\table\Page;
use app\models\table\Post;
use app\models\table\Alias;

/**
 * PageUrl
 *
 * Url rule
 *
 * @author Esipov Alexander <esasser@yandex.ru>
 * @version 1.0
 */
class PageUrl extends UrlRule
{
    /**
     * @var string
     */
    public $connectionID = 'db';

    /**
     * Initializes this rule.
     * @return string
     */
    public function init()
    {
        if ($this->name === null) {
            $this->name = __CLASS__;
        }
    }

    /**
     * Displays page
     * @param Yii\web\UrlManager $manager the URL manager
     * @param object $request
     * @return array|bool
     */
    public function parseRequest($manager, $request)
    {
        $pathInfo   =   $request->getPathInfo();
        $explode    =   explode('/', $pathInfo);

        if ($explode[0] == 'regatta' && count($explode) > 1) {
            if (substr($pathInfo, strlen($pathInfo)-1) == "/")
                $pathInfo = substr($pathInfo,0,strlen($pathInfo)-1);

            $model = Alias::findOne(['url' => $pathInfo]);

            if ($model !== null) {
                $modelPage = Page::findOne([
                    'aliasId'   =>  $model->attributes['id'],
                    'status'    =>  'publish'
                ]);

                if ($modelPage !== null) {
                    $route  =   'page/view';
                    $params =   ['id' => $modelPage->attributes['id'], 'type' => 'page'];

                    return [$route, $params];
                } else {
                    $modelPost = Post::findOne([
                        'aliasId'   =>  $model->attributes['id'],
                        'status'    =>  'publish'
                    ]);
                    if ($modelPost !== null) {
                        $route  =   'page/view';
                        $params =   ['id' => $modelPost->attributes['id'], 'type' => 'post'];

                        return [$route, $params];
                    }
                }
            } else {
                $model = Alias::findOne(['url' => $explode[1]]);
                if ($model !== null) {
                    $modelPage = Page::findOne([
                        'aliasId'   =>  $model->attributes['id'],
                        'status'    =>  'publish'
                    ]);
                    if ($modelPage !== null) {
                        $route  =   'page/view';
                        $params =   ['id' => $modelPage->attributes['id'], 'type' => 'page'];

                        return [$route, $params];
                    } else {
                        $modelPost = Post::findOne([
                            'aliasId'   =>  $model->attributes['id'],
                            'status'    =>  'publish'
                        ]);
                        if ($modelPost !== null) {
                            $route  =   'page/view';
                            $params =   ['id' => $modelPost->attributes['id'], 'type' => 'post'];

                            return [$route, $params];
                        }
                    }
                }
            }
        } else {
            $pathInfo   =   $explode[0];
            $iCount     =   count($pathInfo);
            if ($iCount === 1) {
                $model = Alias::findOne(['url' => $pathInfo]);
                if ($model !== null) {
                    $modelPage = Page::findOne([
                        'aliasId'   =>  $model->attributes['id'],
                        'status'    =>  'publish'
                    ]);
                    if ($modelPage !== null) {
                        $route  =   'page/view';
                        $params =   ['id' => $modelPage->attributes['id'], 'type' => 'page'];

                        return [$route, $params];
                    } else {
                        $modelPost = Post::findOne([
                            'aliasId'   =>  $model->attributes['id'],
                            'status'    =>  'publish'
                        ]);
                        if ($modelPost !== null) {
                            $route  =   'page/view';
                            $params =   ['id' => $modelPost->attributes['id'], 'type' => 'post'];

                            return [$route, $params];
                        }
                    }
                }
            }
        }

        return false;
    }
}
<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id'            =>  'mobilbc',
    'layout'        =>  false,
    'language'      =>  'ru-RU',
    'charset'       =>  'utf-8',
    'defaultRoute'  =>  'site',
    'basePath'      =>  dirname(__DIR__),
    'bootstrap'     =>  ['log'],
    'components' => [
        'request'   =>  [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'   =>  'eKpYB0kKBzlDX47ICZgwMEz638y01wIn',
            'baseUrl'               =>  '',
        ],
        'reCaptcha' =>  [
            'name'      =>  'reCaptcha',
            'class'     =>  'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey'   =>  '6LcseyAUAAAAAITuUgIIR8XEBVy8QFtR6tcf9YaY',
            'secret'    =>  '6LcseyAUAAAAADJb--Sdqka8kYPSJ-5dQk483snS',
        ],
        'formatter' =>  [
            'defaultTimeZone'   =>  'Europe/Moscow',
            'dateFormat'        =>  'dd.MM.yyyy',
            'datetimeFormat'    =>  'dd.MM.yyyy H:mm',
            'decimalSeparator'  =>  ',',
            'thousandSeparator' =>  ' ',
            'currencyCode'      =>  'RUR',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass'     =>  'app\models\table\User',
            'enableAutoLogin'   =>  true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class'             =>  'yii\swiftmailer\Mailer',
            'useFileTransport'  =>  false,
            /*'transport'         =>  [
                'class'         =>  'Swift_SmtpTransport',
                'host'          =>  'smtp.gmail.com',
                'username'      =>  'yesipov94@gmail.com',
                'password'      =>  'gvhnfxgldykdrqzb',
                'port'          =>  '465',
                'encryption'    =>  'ssl',
            ],*/
        ],
        'log' => [
            'traceLevel'    =>  YII_DEBUG ? 3 : 0,
            'flushInterval' =>  1,
            'targets'       =>  [
                [
                    'class'             =>  'yii\log\FileTarget',
                    'levels'            =>  ['error', 'warning'],
                    'exportInterval'    =>  1,
                    'logVars'           =>  [null],
                    'logFile'           =>  '@app/runtime/logs/app/error.log',
                    'maxFileSize'       =>  102400,
                    'maxLogFiles'       =>  10,
                ],
                [
                    'class'             =>  'yii\log\FileTarget',
                    'levels'            =>  ['info'],
                    'exportInterval'    =>  1,
                    'logVars'           =>  [null],
                    'logFile'           =>  '@app/runtime/logs/app/info.log',
                    'maxFileSize'       =>  102400,
                    'maxLogFiles'       =>  10,
                ],
                [
                    'class'             =>  'yii\log\FileTarget',
                    'levels'            =>  ['error', 'warning'],
                    'exportInterval'    =>  1,
                    'logVars'           =>  [null],
                    'categories'        =>  ['yii\db\*'],
                    'logFile'           =>  '@app/runtime/logs/db/error.log',
                    'maxFileSize'       =>  20480,
                    'maxLogFiles'       =>  5,
                ],
                [
                    'class'             =>  'yii\log\FileTarget',
                    'levels'            =>  ['info', 'trace', 'profile'],
                    'exportInterval'    =>  1,
                    'logVars'           =>  [null],
                    'categories'        =>  ['yii\db\*'],
                    'logFile'           =>  '@app/runtime/logs/db/info.log',
                    'maxFileSize'       =>  20480,
                    'maxLogFiles'       =>  5,
                ],
                [
                    'class'             =>  'yii\log\FileTarget',
                    'levels'            =>  ['info'],
                    'exportInterval'    =>  1,
                    'categories'        =>  ['cron'],
                    'logFile'           =>  '@app/runtime/logs/cron/info.log',
                ],
                [
                    'class'             =>  'yii\log\FileTarget',
                    'levels'            =>  ['error', 'warning'],
                    'exportInterval'    =>  1,
                    'categories'        =>  ['cron'],
                    'logFile'           =>  '@app/runtime/logs/cron/error.log',
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl'   =>  true,
            'showScriptName'    =>  false,
            'suffix'            =>  '/',
            'normalizer'        =>  [
                'class'     =>  'yii\web\UrlNormalizer',
                'action'    =>  yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY, // используем временный редирект вместо постоянного
            ],
            'rules'             =>  [
                'admin'                         =>  'admin/index',
                'admin/edit/<type>/<id:\d+>/'   =>  'admin/edit',
                'admin/delete/<type>/<id:\d+>/' =>  'admin/delete',
                'admin/save/<type>/'            =>  'admin/save',
                'admin/create/<type>/'          =>  'admin/create',
                'admin/create/<type>/<id:\d+>/' =>  'admin/create',
                'admin/upload/<id:\d+>/'        =>  'admin/upload',
                'admin/uploads/<id:\d+>/'       =>  'admin/uploads',
                'site/login'                    =>  'admin/login',
                'admin/posts'                   =>  'admin/posts',
                'admin/pages'                   =>  'admin/pages',
                'regatta/post/<id:\d+>'         =>  'site/post',
                'regatta'                       =>  'site/regata',
                'sendmail'                      =>  'site/sendmail/',
                'regatta/gallery/<id:\d+>/'     =>  'site/gallery/',
                'regatta/albums/<id:\d+>/'      =>  'site/albums/',
                'ajax/albums/<id:\d+>/'         =>  'site/ajax/',
                'captcha'                       =>  'site/captcha',
                [
                    'class'         =>  'app\components\PageUrl',
                    'connectionID'  =>  'db',
                ],
                [
                    'pattern'       =>  'sitemap',
                    'route'         =>  'sitemap/index',
                    'suffix'        =>  '.xml',
                    'normalizer'    =>  false, // отключаем нормализатор для этого правила
                ],
            ],
        ],
        'assetManager' => [
            'class'         =>  'yii\web\AssetManager',
            'linkAssets'    =>  true,
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;

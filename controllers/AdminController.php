<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;

use yii\web\UploadedFile;

use app\models\form\LoginForm;

use app\models\table\Alias;
use app\models\table\Post;
use app\models\table\Page;
use app\models\table\Block;
use app\models\table\SubBlock;
use app\models\table\Object;
use app\models\table\Meta;
use app\models\table\Settings;
use app\models\table\FeedBack;
use app\models\table\Albums;
use app\models\table\GalleryImages;

use app\models\form\PostForm;
use app\models\form\PageForm;
use app\models\form\BlockForm;
use app\models\form\SubBlockForm;
use app\models\form\ObjectForm;
use app\models\form\MetaForm;
use app\models\form\SettingsForm;
use app\models\form\AlbumsForm;
use app\models\form\GalleryImagesForm;
use app\models\form\UploadForm;

/**
 * AdminController
 *
 * Administration app in admin panel
 *
 * @author Esipov Alexander <esasser@yandex.ru>
 * @version 1.0
 */
class AdminController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'admin';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' =>  AccessControl::className(),
                'rules' =>  [
                    [
                        'actions'   =>  ['logout', 'admin', 'index', 'edit', 'create', 'save', 'delete', 'upload', 'admin/upload', 'uploads', 'admin/uploads', 'admin/logout', 'admin/index', 'admin/edit', 'admin/create', 'admin/save', 'admin/delete', 'admin/posts', 'posts', 'admin/pages', 'pages'],
                        'allow'     =>  true,
                        'roles'     =>  ['@'],
                    ],
                    [
                        'actions'   =>  ['admin', 'index', 'edit', 'create', 'save', 'delete', 'admin/index', 'admin/edit', 'admin/create', 'admin/save', 'admin/delete', 'upload', 'admin/upload', 'uploads', 'admin/uploads', 'admin/posts', 'posts', 'admin/pages', 'pages'],
                        'allow'     =>  false,
                        'roles'     =>  ['?'],
                    ],
                    [
                        'actions'   =>  ['login', 'admin/login'],
                        'allow'     =>  true,
                        'roles'     =>  ['?'],
                    ],
                    [
                        'actions'   =>  ['error'],
                        'allow'     =>  true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $this->getView()->title = 'Ошибка';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'             =>  'yii\captcha\CaptchaAction',
                'fixedVerifyCode'   =>  YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Check admin user
     *
     * @return string
     */
    public function checkUser()
    {
        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect('/admin/login/');
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect('/admin/');
        }

        $this->getView()->title =   'Вход в панель администратора';
        $model                  =   new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return Yii::$app->response->redirect('/admin/');
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays admin index page.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->checkUser();
        $this->getView()->title =   'Главная | Панель администратора';
        $data                   =   $this->getAllData();
        return $this->render('index', $data);
    }

    /**
     * Get all models data.
     *
     * @return array
     */
    public function getAllData()
    {
        /* all post */
        $postClass                      =   new Post();
        $result['posts']                =   $postClass->find()->all();
        $result['postLabels']           =   $postClass->attributeLabels();

        /* all page */
        $pageClass                      =   new Page();
        $result['pages']                =   $pageClass->find()->where('id not in (8)')->all();
        $result['pageLabels']           =   $pageClass->attributeLabels();

        /* main page */
        $mainPageClass                  =   new Page();
        $result['mainPage']             =   Page::find()->where(['id' => 1])->one();
        $result['mainPageLabels']       =   $mainPageClass->attributeLabels();

        /* refata page */
        $regataPageClass                =   new Page();
        $result['regataPage']           =   Page::find()->where(['id' => 2])->one();
        $result['regataPageLabels']     =   $regataPageClass->attributeLabels();

        /* news page */
        $newsPageClass                  =   new Page();
        $result['newsPage']             =   Page::find()->where(['id' => 8])->one();
        $result['newsPageLabels']       =   $newsPageClass->attributeLabels();

        /* all object */
        $objectClass                    =   new Object();
        $result['objects']              =   $objectClass->find()->all();
        $result['objectLabels']         =   $objectClass->attributeLabels();

        /* all block */
        $blockClass                     =   new Block();
        $result['blocks']               =   $blockClass->find()->all();
        $result['blockLabels']          =   $blockClass->attributeLabels();

        /* main page block */
        $blockMainClass                 =   new Block();
        $result['blockMain']            =   Block::find()->where(['pageId' => 1])->orWhere(['pageId' => 0])->all();
        $result['blockMainLabels']      =   $blockMainClass->attributeLabels();

        /* regata page block */
        $blockRegataClass               =   new Block();
        $result['blockRegata']          =   Block::find()->where(['pageId' => 2])->orWhere(['pageId' => 0])->all();
        $result['blockRegataLabels']    =   $blockRegataClass->attributeLabels();

        /* common page block */
        $blockCommonClass               =   new Block();
        $result['blockCommon']          =   Block::find()->where(['pageId' => 0])->all();
        $result['blockCommonLabels']    =   $blockCommonClass->attributeLabels();

        /* all SubBlock */
        $subBlockClass                  =   new SubBlock();
        $result['subBlocks']            =   $subBlockClass->find()->all();
        $result['subBlockLabels']       =   $subBlockClass->attributeLabels();

        /* all alias */
        $aliasClass                     =   new Alias();
        $result['alias']                =   $aliasClass->find()->all();
        $result['aliasLabels']          =   $aliasClass->attributeLabels();

        /* all meta */
        $metaClass                      =   new Meta();
        $result['metas']                =   $metaClass->find()->all();
        $result['metaLabels']           =   $metaClass->attributeLabels();

        /* all settings */
        $settingsClass                  =   new Settings();
        $result['settings']             =   $settingsClass->find()->all();
        $result['settingsLabels']       =   $settingsClass->attributeLabels();

        /* feedback */
        $feedbackClass                  =   new Feedback();
        $result['feedbacks']            =   $feedbackClass->find()->all();
        $result['feedbackLabels']       =   $feedbackClass->attributeLabels();

        /* albums */
        $albumsClass                    =   new Albums();
        $result['albums']               =   $albumsClass->find()->all();
        $result['albumsLabels']         =   $albumsClass->attributeLabels();

        /* gallery images */
        $giClass                        =   new GalleryImages();
        $result['galleryImages']        =   $giClass->find()->all();
        $result['galleryImagesLabels']  =   $giClass->attributeLabels();

        return $result;
    }

    /**
     * Displays edit page.
     *
     * @return string
     */
    public function actionEdit()
    {
        $this->checkUser();
        $this->getView()->title = 'Страница редактирования | Панель администратора';

        $id             =   Yii::$app->request->get('id');
        $attr['type']   =   Yii::$app->request->get('type');
        $attr['data']   =   $this->getAllData();

        if ($attr['type'] == 'albums') {
            $attr['albumsId']   =   $id;
        }

        if ($attr['type'] == 'page') {
            $modelForm  =   new PageForm();
            $model      =   Page::find()->where('id = :id', [':id' => $id])->one();
        } elseif ($attr['type'] == 'post') {
            $modelForm  =   new PostForm();
            $model      =   Post::find()->where('id = :id', [':id' => $id])->one();
        } elseif ($attr['type'] == 'object') {
            $modelForm  =   new ObjectForm();
            $model      =   Object::find()->where('id = :id', [':id' => $id])->one();
        } elseif ($attr['type'] == 'block') {
            $modelForm  =   new BlockForm();
            $model      =   Block::find()->where('id = :id', [':id' => $id])->one();
        } elseif ($attr['type'] == 'sub_block') {
            $modelForm  =   new SubBlockForm();
            $model      =   SubBlock::find()->where('id = :id', [':id' => $id])->one();
        } elseif ($attr['type'] == 'meta') {
            $modelForm  =   new MetaForm();
            $model      =   Meta::find()->where('id = :id', [':id' => $id])->one();
        } elseif ($attr['type'] == 'settings') {
            $modelForm  =   new SettingsForm();
            $model      =   Settings::find()->where('id = :id', [':id' => $id])->one();
        } elseif ($attr['type'] == 'albums') {
            $modelForm  =   new AlbumsForm();
            $model      =   Albums::find()->where('id = :id', [':id' => $id])->one();
        } elseif ($attr['type'] == 'gi') {
            $modelForm  =   new GalleryImagesForm();
            $model      =   GalleryImages::find()->where('id = :id', [':id' => $id])->one();
        } else {
            $modelForm  =   null;
            $model      =   null;
        }

        if (is_null($modelForm) || is_null($model)) {
            return Yii::$app->response->redirect('/admin/create/' . $attr['type'] . '/');
        }

        $attr['model']      =   $model;
        $attr['modelForm']  =   $modelForm;
        $attr['labels']     =   $model->attributeLabels();

        return $this->render('edit', $attr);
    }

    /**
     * Displays create page.
     *
     * @return string
     */
    public function actionCreate()
    {
        $this->checkUser();
        $this->getView()->title = 'Страница создания | Панель администратора';
        $attr['type']   =   Yii::$app->request->get('type');
        $attr['data']   =   $this->getAllData();

        if ($attr['type'] == 'gi') {
            $attr['albumsId']   =   Yii::$app->request->get('id');
        }

        if ($attr['type'] == 'page') {
            $modelForm  =   new PageForm();
        } elseif ($attr['type'] == 'post') {
            $modelForm  =   new PostForm();
        } elseif ($attr['type'] == 'object') {
            $modelForm  =   new ObjectForm();
        } elseif ($attr['type'] == 'block') {
            $modelForm  =   new BlockForm();
        } elseif ($attr['type'] == 'sub_block') {
            $modelForm  =   new SubBlockForm();
        } elseif ($attr['type'] == 'settings') {
            $modelForm  =   new SettingsForm();
        } elseif ($attr['type'] == 'meta') {
            $modelForm  =   new MetaForm();
        } elseif ($attr['type'] == 'albums') {
            $modelForm  =   new AlbumsForm();
        } elseif ($attr['type'] == 'gi') {
            $modelForm  =   new GalleryImagesForm();
        } else {
            $modelForm  =   null;
        }

        if (is_null($modelForm)) {
            return Yii::$app->response->redirect('/admin/create/' . $attr['type'] . '/');
        }

        $attr['model']      =   $modelForm;
        $attr['labels']     =   $modelForm->attributeLabels();

        return $this->render('create', $attr);
    }

    /**
     * Save record.
     *
     * @return string
     */
    public function actionSave()
    {
        $this->checkUser();
        if (Yii::$app->request->isAjax) {
            $type = Yii::$app->request->get('type');

            if ($type == 'sub_block') {
                $typeFormData = 'SubBlock';
            } elseif ($type == 'gi') {
                $typeFormData = 'GalleryImages';
            } else {
                $typeFormData = ucfirst($type);
            }

            $formData = Yii::$app->request->post($typeFormData);

            if (is_null($formData)) {
                $formData = Yii::$app->request->post($typeFormData . 'Form');
            }

            $result['success']  =   0;
            $saveTemplate       =   $formData['saveTemplate'];
            unset($formData['saveTemplate']);

            if ($type == 'page') {
                $model = new Page();
            } elseif ($type == 'post') {
                $model = new Post();
            } elseif ($type == 'object') {
                $model = new Object();
            } elseif ($type == 'block') {
                $model = new Block();
            } elseif ($type == 'sub_block') {
                $model = new SubBlock();
            } elseif ($type == 'meta') {
                $model = new Meta();
            } elseif ($type == 'settings') {
                $model = new Settings();
            } elseif ($type == 'albums') {
                $model = new Albums();
            } elseif ($type == 'gi') {
                $model = new GalleryImages();
            }

            $coloumns = $model->attributeLabels();

            if ($type == 'page' || $type == 'post') {
                $modelAlias = new Alias();

                if ((int)$formData['aliasId'] == 0) {
                    $CheckExistUrl = $modelAlias->findOne(['url' => $formData['url'],]);

                    if (is_null($CheckExistUrl)) {
                        $modelAlias->setAttributes([
                            'id'    =>  NULL,
                            'url'   =>  $formData['url']
                        ]);

                        if ($modelAlias->save()) {
                            $formData['aliasId'] = $modelAlias->attributes['id'];
                        }
                    } else {
                        $result['error'] = 'error';
                        return json_encode($result);
                    }
                } else {
                    $modelAlias = $modelAlias->findOne(['id' => $formData['aliasId']]);

                    if ($modelAlias->attributes['url'] !== $formData['url']) {
                        $CheckExistUrl = $modelAlias->findOne(['url' => $formData['url'],]);

                        if (is_null($CheckExistUrl)) {
                            $modelAlias->setAttributes(['url' => $formData['url']]);

                            $modelAlias->save();
                        } else {
                            $result['error'] = 'error';
                            return json_encode($result);
                        }
                    }
                }

                unset($formData['url']);
            }

            if ($saveTemplate == 'edit') {
                $model = $model->findOne(['id' => $formData['id']]);
                foreach ($coloumns as $key => $value) {
                    $attr[$key] = $formData[$key];
                }

                $model->setAttributes($attr, false);
            } else {
                if ($type == 'albums') {
                    if (empty($formData['onRegataPage'])) {
                        $formData['onRegataPage'] = 0;
                    }
                    $lastId = Albums::find()->where(['id' => Albums::find()->max('id')])->one()->attributes['id'];
                    $formData['id'] = $lastId + 1;
                    $formData['name'] = 'albums_' . $formData['id'];
                    FileHelper::createDirectory("uploads/gallery/" . $formData['name'], $mode = 0755, $recursive = true);
                }

                foreach ($coloumns as $key => $value) {
                    $attr[$key] = $formData[$key];
                }

                $model->setAttributes($attr, false);
            }

            if ($model->validate()) {
                if (!$model->save()) {
                    $result['error']    =   $model->getErrors();
                } else {
                    $result['success']  =   1;
                }
            }

            return json_encode($result);
        }
    }

    /**
     * Delete record
     *
     * @return string
     */
    public function actionDelete()
    {
        $this->checkUser();
        $id     =   Yii::$app->request->get('id');
        $type   =   Yii::$app->request->get('type');

        if ($type == 'page') {
            $model = new Page();
        } elseif ($type == 'post') {
            $model = new Post();
        } elseif ($type == 'object') {
            $model = new Object();
        } elseif ($type == 'block') {
            $model = new Block();
        } elseif ($type == 'sub_block') {
            $model = new SubBlock();
        } elseif ($type == 'meta') {
            $model = new Meta();
        } elseif ($type == 'settings') {
            $model = new Settings();
        } elseif ($type == 'albums') {
            $model = new Albums();
        } elseif ($type == 'gi') {
            $model = new GalleryImages();
        }

        $CheckExisting = $model->find()->where(['id' => $id])->one();

        if (!is_null($CheckExisting)) {
            if ($type == 'page') {
                $modelExtra1 = new Alias();
                $modelExtra1->deleteAll('id = :id', [':id' => $CheckExisting->attributes['aliasId']]);

                $modelExtra3 = new Block();
                $modelExtra = $modelExtra3->find()->where(['pageId' => $id])->all();

                $modelExtra2 = new SubBlock();
                foreach ($modelExtra as $block) {
                    $modelExtra2->deleteAll('blockId = :id', [':id' => $block->attributes['id']]);
                }

                $modelExtra3->deleteAll('pageId = :id', [':id' => $id]);
            } elseif ($type == 'post') {
                $modelExtra4 = new Alias();
                $modelExtra4->deleteAll('id = :id', [':id' => $CheckExisting->attributes['aliasId']]);
            } elseif ($type == 'block') {
                $modelExtra5 = new SubBlock();
                $modelExtra5->deleteAll('blockId = :id', [':id' => $id]);
            } elseif ($type == 'albums') {
                $modelExtra6 = new GalleryImages();
                $modelExtra6->deleteAll('albumsId = :id', [':id' => $id]);
                FileHelper::removeDirectory('/uploads/gallery/' . $CheckExisting->attributes['name']);
            }

            if ($model->deleteAll('id = :id', [':id' => $id])) {
                if (Yii::$app->request->isAjax) {
                    return $id;
                } else {
                    return Yii::$app->response->redirect('/admin/');
                }
            }
        }
    }

    /**
     * Get All post
     *
     * Ajax query
     *
     * @return string
     */
    public function actionPosts()
    {
        if (Yii::$app->request->isAjax) {
            $model = Post::find()->all();
            foreach ($model as $key => $item) {
                $allPosts[$key] = $item->attributes;
            }
            return json_encode($allPosts);
        }
    }

    /**
     * Get All page
     *
     * Ajax query
     *
     * @return string
     */
    public function actionPages()
    {
        if (Yii::$app->request->isAjax) {
            $model = Page::find()->all();
            foreach ($model as $key => $item) {
                $allPages[$key] = $item->attributes;
            }
            return json_encode($allPages);
        }
    }

    /**
     * @return string
     */
    public function actionUpload()
    {
        $albumsId = Yii::$app->request->get('id');
        $model = new UploadForm();

        return $this->render('upload', [
            'model'     =>  $model,
            'albumsId'  =>  $albumsId
        ]);
    }

    /**
     *
     * @return string
     */
    public function actionUploads()
    {
        $albumsId   =   Yii::$app->request->get('id');
        $uploadForm =   new UploadForm();

        if (Yii::$app->request->isPost) {
            $uploadForm->imageFiles =   UploadedFile::getInstances($uploadForm,'imageFiles');
            $dirUpload              =   'gallery/albums_' . $albumsId . '/';

            if ($uploadForm->upload($dirUpload, $albumsId)) {
                return Yii::$app->response->redirect('/admin/upload/' . $albumsId);
            }
        }
    }
}
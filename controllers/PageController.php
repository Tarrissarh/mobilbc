<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\FileHelper;

use app\models\table\Alias;
use app\models\table\Meta;
use app\models\table\Page;
use app\models\table\Post;
use app\models\table\Block;
use app\models\table\Settings;
use app\models\table\SubBlock;
use app\models\table\Albums;
use app\models\form\ContactForm;

/**
 * PageController
 *
 * Page controller for all page in database
 *
 * @author Esipov Alexander <esasser@yandex.ru>
 * @version 1.0
 */
class PageController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'site';

    /**
     * @var array
     */
    public $attr;

    /**
     * Displays page
     * @param integer $id
     * @param string $type
     * @return string
     */
    public function actionView($id, $type)
    {
        $modelContact = new ContactForm();

        $settings = Settings::find()->all();
        foreach ($settings as $setting) {
            Yii::$app->params[$setting->attributes['name']] = $setting->attributes['value'];
        }
        
        $albums = array();

        if ($type == 'post') {
            $model = Post::findOne([
                'id'        =>  $id,
                'status'    =>  'publish'
            ]);
        } elseif ($type == 'page') {
            $model = Page::findOne([
                'id'        =>  $id,
                'status'    =>  'publish'
            ]);

            if ($model->attributes['name'] == 'regatta_photoreports') {
                $albums = Albums::find()->where(['status' => 1])->all();
            }

            $modelBlock     =   Block::find()->where(['pageId' => $id])->all();
            $blocks         =   null;
            if ($modelBlock !== null) {
                $blocks = array();
                foreach ($modelBlock as $key => $item) {
                    $blocks[$key]['block']  =   $item->attributes;
                    $subBlocks              =   array();
                    $modelSubBlock          =   SubBlock::find()->where(['blockId' => $item->attributes['id']])->all();
                    foreach ($modelSubBlock as $k => $i) {
                        $subBlocks[$key]['sub_block'][$k] = $i->attributes;
                    }
                }
            }
            $attr['blocks'] = $blocks;
        }

        $metas = Meta::find()->where([
            'typeId'    =>  $model->attributes['id'],
            'type'      =>  $type
        ])->all();

        $pages = Page::find()->where(['onMenu' => 1])->all();

        foreach ($pages as $key => $page) {
            if ($type == 'post' || stristr($model->attributes['name'], 'regatta') !== false) {
                if (stristr($page->attributes['name'], 'regatta') !== false) {
                    $menu[$key]['title']    =   $page->attributes['title'];
                    $menu[$key]['url']      =   Alias::findOne(['id' => $page->attributes['aliasId']])->attributes['url'];
                }
            } else {
                if (stristr($page->attributes['name'], 'regatta') === false) {
                    $menu[$key]['title']    =   $page->attributes['title'];
                    $menu[$key]['url']      =   Alias::findOne(['id' => $page->attributes['aliasId']])->attributes['url'];
                }
            }
        }

        $modelAlias         =   Alias::findOne(['id' => $model->attributes['aliasId']]);
        $dataPage           =   $model->attributes;
        $dataPage['url']    =   $modelAlias['url'];
        unset($dataPage['aliasId']);

        if ($type == 'post') {
            $dataPage['name'] = 'regatta_posts';
        }

        $attr = [
            'modelContact'  =>  $modelContact,
            'model'     =>  $model,
            'page'      =>  $dataPage,
            'metas'     =>  $metas,
            'menu'      =>  $menu,
            'albums'    =>  $albums
        ];
        $this->attr     =   $attr;
        return $this->render($this->getTemplate($type, $model), $attr);
    }

    /**
     * Get template for page
     * @param string $type
     * @param object $model
     * @return string
     */
    public function getTemplate($type, $model)
    {
        $files = FileHelper::findFiles(
            FileHelper::normalizePath('../views/site/' . $type . '_template'),
            [
                'only' => ['*.php'],
                'recursive' => true
            ]
        );
        foreach ($files as $key => $file) {
            $files[$key] = explode('.', explode('\\', $file)[4])[0];
        }

        if (array_search($model->attributes['template'], $files) !== false) {
            $template = $type . '_template/' . $model->attributes['template'];
        } else {
            $template = $type . '_template/main';
        }

        return '/site/' . $template;
    }
}
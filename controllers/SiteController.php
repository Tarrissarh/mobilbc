<?php

namespace app\controllers;

use app\models\table\Albums;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;

use app\models\form\ContactForm;

use app\models\table\Post;
use app\models\table\Page;
use app\models\table\Block;
use app\models\table\SubBlock;
use app\models\table\Object;
use app\models\table\Alias;
use app\models\table\Meta;
use app\models\table\FeedBack;
use app\models\table\Settings;
use app\models\table\GalleryImages;

/**
 * SiteController
 *
 * Main app controller for index, regata, post by id, ajax sendmail
 *
 * @author Esipov Alexander <esasser@yandex.ru>
 * @version 1.0
 */
class SiteController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'site';

    /**
     * @var array
     */
    public $attr;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $settings = Settings::find()->all();
        foreach ($settings as $setting) {
            Yii::$app->params[$setting->attributes['name']] = $setting->attributes['value'];
        }

        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $this->getView()->title = 'Ошибка';

        $pages = Page::find()->where(['onMenu' => 1])->all();

        foreach ($pages as $key => $page) {
            if (stristr($page->attributes['name'], 'regatta') === false) {
                $menu[$key]['title']    =   $page->attributes['title'];
                $menu[$key]['url']      =   Alias::findOne(['id' => $page->attributes['aliasId']])->attributes['url'];
            }
        }
        $pathInfo   =   Yii::$app->request->getUrl();
        $explode    =   explode('/', $pathInfo);


        if (array_search('regatta', $explode) !== false) {
            $this->attr['page']['name'] = 'regatta';
        } else {
            $this->attr['page']['name'] = 'main';
        }
        $this->attr['menu'] = $menu;

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'             =>  'yii\captcha\CaptchaAction',
                'fixedVerifyCode'   =>  YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $modelContact = new ContactForm();

        $files = FileHelper::findFiles(FileHelper::normalizePath('../views/site/page_template'), [
            'only'      =>  ['*.php'],
            'recursive' =>  true
        ]);

        foreach ($files as $key => $file) {
            $files[$key] = explode('.', explode('\\', $file)[4])[0];
        }

        if (array_search('front', $files) !== false) {
            $main_template = 'page_template/front';
        } else {
            $main_template = 'index';
        }

        $pages = Page::find()->where(['onMenu' => 1])->all();

        foreach ($pages as $key => $page) {
            if (stristr($page->attributes['name'], 'regatta') === false) {
                $menu[$key]['title']    =   $page->attributes['title'];
                $menu[$key]['url']      =   Alias::findOne(['id' => $page->attributes['aliasId']])->attributes['url'];
            }
        }

        $page = Page::findOne([
            'name'      =>  'main',
            'aliasId'   =>  1
        ]);

        $this->getView()->title = $page->attributes['title'];

        $metas = Meta::find()->where([
            'typeId'    =>  $page->attributes['id'],
            'type'      =>  'page'
        ])->all();

        $block          =   Block::find()->where(['pageId' => $page->attributes['id']])->all();
        $blocks         =   $this->getSubBlock($block);
        $blockCommon    =   Block::find()->where(['pageId' => 0])->all();
        $blocksCommon   =   $this->getSubBlock($blockCommon);

        $attr = [
            'modelContact'  =>  $modelContact,
            'page'          =>  $page->attributes,
            'blocks'        =>  $blocks,
            'blocksCommon'  =>  $blocksCommon,
            'menu'          =>  $menu,
            'metas'         =>  $metas,
        ];

        $this->attr = $attr;
        return $this->render($main_template, $attr);
    }

    /**
     * Displays regatapage.
     *
     * @return string
     */
    public function actionRegata()
    {
        $modelContact = new ContactForm();

        $files = FileHelper::findFiles(FileHelper::normalizePath('../views/site/page_template'), [
            'only'      =>  ['*.php'],
            'recursive' =>  true
        ]);

        foreach ($files as $key => $file) {
            $files[$key] = explode('.', explode('\\', $file)[4])[0];
        }

        if (array_search('regatta', $files) !== false) {
            $main_template = 'page_template/regatta';
        } else {
            $main_template = 'index';
        }

        $pages = Page::find()->where(['onMenu' => 1])->all();

        foreach ($pages as $key => $page) {
            if (stristr($page->attributes['name'], 'regatta') !== false) {
                $menu[$key]['title']    =   $page->attributes['title'];
                $menu[$key]['url']      =   Alias::findOne(['id' => $page->attributes['aliasId']])->attributes['url'];
            }
        }

        $page = Page::findOne([
            'name'      =>  'regatta',
            'aliasId'   =>  2
        ]);

        $this->getView()->title = $page->attributes['title'];

        $metas = Meta::find()->where([
            'typeId'    =>  $page->attributes['id'],
            'type'      =>  'page'
        ])->all();

        $block          =   Block::find()->where(['pageId' => $page->attributes['id']])->all();
        $blocks         =   $this->getSubBlock($block);
        $blockCommon    =   Block::find()->where(['pageId' => 0])->all();
        $blocksCommon   =   $this->getSubBlock($blockCommon);
        $posts[0]       =   Post::find()->orderBy(['id' => SORT_DESC])->one();
        $posts[1]       =   Post::find()->orderBy(['id' => SORT_DESC])->offset(1)->one();

        if (!is_null($posts[0])) {
            $alias[0] = Alias::findOne(['id' => $posts[0]->attributes['aliasId']]);
        } else {
            $alias[0] = null;
        }

        if (!is_null($posts[1])) {
            $alias[1] = Alias::findOne(['id' => $posts[1]->attributes['aliasId']]);
        } else {
            $alias[1] = null;
        }

        if (!is_null($posts)) {
            foreach ($posts as $key => $post) {
                if (!is_null($post)) {
                    $allPost[$key] = $post->attributes;
                    unset($allPost[$key]['aliasId']);
                    $allPost[$key]['url'] = $alias[$key]->attributes['url'];
                } else {
                    $allPost[$key] = null;
                }
            }
        } else {
            $allPost = null;
        }

        $album = Albums::find()->where([
            'onRegataPage'  =>  1,
            'status'        =>  1
        ])->one();

        if (!is_null($album)) {
            $galleryImages = GalleryImages::find()->where([
                'albumsId'  =>  $album->attributes['id'],
                'status'    =>  1
            ])->all();
        } else {
            $galleryImages = null;
        }



        $attr = [
            'modelContact'  =>  $modelContact,
            'page'          =>  $page->attributes,
            'blocks'        =>  $blocks,
            'blocksCommon'  =>  $blocksCommon,
            'menu'          =>  $menu,
            'metas'         =>  $metas,
            'posts'         =>  $allPost,
            'galleryImages' =>  $galleryImages,
            'album'         =>  $album,
        ];
        $this->attr = $attr;
        return $this->render($main_template, $attr);
    }

    /**
     * Get SubBlock
     * @param object $block
     * @return array
     */
    public function getSubBlock($block)
    {
        if (!is_null($block) && !empty($block)) {
            foreach ($block as $key => $item) {
                $blocks[$key]['block']  =   $item->attributes;
                $sub_block              =   SubBlock::find()->where(['blockId' => $item->attributes['id']])->all();
                $sub_blocks             =   array();

                foreach ($sub_block as $k => $i) {
                    $sub_blocks[$k] = $i->attributes;
                }

                $blocks[$key]['sub_block'] = $sub_blocks;
            }
        } else {
            $blocks = NULL;
        }

        return $blocks;
    }

    /**
     * Displays post by ID
     * @throws NotFoundHttpException
     * @return string
     */
    public function actionPost()
    {
        $post = Post::findOne(['id' => Yii::$app->request->get('id')]);
        if (!is_null($post)) {
            $this->getView()->title = $post->attributes['title'];
            $files = FileHelper::findFiles(FileHelper::normalizePath('../views/site/page_template'), [
                'only' => ['*.php'],
                'recursive' => true
            ]);

            foreach ($files as $key => $file) {
                $files[$key] = explode('.', explode('\\', $file)[4])[0];
            }

            if (array_search($post->attributes['template'], $files) !== false) {
                $main_template = 'post_template/' . $post->attributes['template'];
            } else {
                $main_template = 'post_template/main';
            }

            $attr = [
                'post' => $post->attributes,
            ];
            $this->attr = $attr;
            return $this->render($main_template, $attr);
        } else {
            throw new NotFoundHttpException('Новость не найдена' ,404);
        }
    }

    /**
     * Get images by albums
     *
     * @return string
     */
    public function actionGallery()
    {
        if (Yii::$app->request->isAjax) {
            $images    =   GalleryImages::find()->where(['albumsId' => Yii::$app->request->get('id')])->all();

            foreach ($images as $key => $image) {
                $gallery[$key] = $image->attributes;
            }

            return json_encode([
                'gallery'   =>  json_encode($gallery)
            ]);
        }
    }

    /**
     * Images albums
     * @throws NotFoundHttpException
     * @return string
     */
    public function actionAlbums()
    {
        $albums    =   Albums::findOne(['id' => Yii::$app->request->get('id')]);
        $images    =   GalleryImages::find()->where(['albumsId' => Yii::$app->request->get('id')])->all();

        if (!is_null($albums)) {
            $this->getView()->title =   $albums->attributes['title'];
            $gallery                =   array();

            if (!is_null($images)) {
                foreach ($images as $key => $image) {
                    $gallery[$key] = $image->attributes;
                }
            }

            $pages = Page::find()->where(['onMenu' => 1])->all();

            foreach ($pages as $key => $page) {
                if (stristr($page->attributes['name'], 'regatta') !== false) {
                    $menu[$key]['title']    =   $page->attributes['title'];
                    $menu[$key]['url']      =   Alias::findOne(['id' => $page->attributes['aliasId']])->attributes['url'];
                }
            }

            $attr = [
                'menu'      =>  $menu,
                'gallery'   =>  $gallery,
                'albums'    =>  $albums->attributes,
                'page'      =>  [
                    'name'  =>  'regatta_albums',
                ],
            ];

            $this->attr = $attr;
            return $this->render('page_template/albums', $attr);
        } else {
            throw new NotFoundHttpException('Альбом не найден' ,404);
        }
    }

    /**
     * Ajax query
     *
     * @return string || null
     */
    public function actionAjax()
    {
        if (Yii::$app->request->isAjax) {
            $images    =   GalleryImages::find()->where(['albumsId' => Yii::$app->request->get('id')])->orderBy(['id' => SORT_ASC])->one();

            if (!is_null($images)) {
                return json_encode([
                    'image'   =>  json_encode($images->attributes)
                ]);
            } else {
                return json_encode([
                    'image'   =>  null
                ]);
            }
        }
    }

    /**
     * Send email
     *
     * @return string
     */
    public function actionSendmail()
    {
        if (Yii::$app->request->isAjax) {
            $contactForm = new ContactForm();

            if ($contactForm->load(Yii::$app->request->post())) {

                $dataForm = Yii::$app->request->post('ContactForm');

                if ($dataForm['subject'] == 'Заявка с сайта БЦ Мобил') {
                    $adminEmail = Yii::$app->params['adminEmail'];
                } else if ($dataForm['subject'] == 'Заявка с сайта Регата Мобил') {
                    $adminEmail = Yii::$app->params['adminEmailRegata'];
                } else if($dataForm['subject'] == 'Заявка помещения') {
                    $adminEmail = Yii::$app->params['adminEmailPremises'];
                }

                $emailsToSend   =   explode(';', $adminEmail);

                if ( $contactForm->contact($emailsToSend) ) {
                    $model      =   new FeedBack();

                    $model->setAttributes([
                        'id'        =>  NULL,
                        'to'        =>  $adminEmail,
                        'from'      =>  $dataForm['email'],
                        'name'      =>  $dataForm['name'],
                        'subject'   =>  $dataForm['subject'],
                        'phone'     =>  $dataForm['phone'],
                        'body'      =>  $dataForm['body'],
                        'created'   =>  date('Y-m-d H:i:s'),
                    ]);
                    if ($model->save()) {
                        return json_encode(['success' => true]);
                    } else {
                        return json_encode([
                            'success'   =>  false,
                            'error'     =>  'not save'
                        ]);
                    }
                } else {
                    return json_encode([
                        'success'   =>  false,
                        'error'     =>  'not send'
                    ]);
                }

            }
        }
    }
}

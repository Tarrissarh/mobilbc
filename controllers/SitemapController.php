<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

use app\models\table\Post;
use app\models\table\Page;
use app\models\table\Alias;

/**
 * SitemapController
 *
 * Sitemap generator
 *
 * @author Esipov Alexander <esasser@yandex.ru>
 * @version 1.0
 */
class SitemapController extends Controller
{
    const ALWAYS    =   'always';
    const HOURLY    =   'hourly';
    const DAILY     =   'daily';
    const WEEKLY    =   'weekly';
    const MONTHLY   =   'monthly';
    const YEARLY    =   'yearly';
    const NEVER     =   'never';

    /**
     * Displays sitemap.xml.
     *
     * @return string
     */
    public function actionIndex()
    {
        $urls       =   array();
        $changeFreq =   self::WEEKLY;
        $priority   =   0.5;
        $host       =   Yii::$app->request->hostInfo;
        $aliases    =   Alias::find()->all();

        foreach ($aliases as $alias) {
            if ($alias->attributes['url'] == '/') {
                $urls[] = array(
                    'loc'           =>  $host . $alias->attributes['url'],
                    'changefreq'    =>  $changeFreq,
                    'priority'      =>  $priority
                );
            } else {
                $pages = Page::find([
                    'aliasId'   =>  $alias->attributes['id'],
                    'status'    =>  'publish'
                ])->one();

                $posts = Post::find([
                    'aliasId'   =>  $alias->attributes['id'],
                    'status'    =>  'publish'
                ])->one();

                if ($pages !== null || $posts !== null) {
                    $urls[] = array(
                        'loc'           =>  $host . '/' . $alias->attributes['url'],
                        'changefreq'    =>  $changeFreq,
                        'priority'      =>  $priority
                    );
                }
            }
        }

        header("Content-type: text/xml");
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        echo $this->generateXML($urls);
        Yii::$app->end();
    }

    /**
     * Generator xml
     * @param array $urls
     * @return string
     */
    public function generateXML($urls)
    {
        $dom    =   new \DomDocument();
        $urlset =   $dom->createElement('urlset');
        $urlset->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');

        foreach($urls as $item) {
            $url = $dom->createElement('url');

            foreach ($item as $key => $value) {
                $elem = $dom->createElement($key);
                $elem->appendChild($dom->createTextNode($value));
                $url->appendChild($elem);
            }

            $urlset->appendChild($url);
        }

        $dom->appendChild($urlset);

        return $dom->saveXML();
    }
}
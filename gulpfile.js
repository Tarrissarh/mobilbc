var gulp = require('gulp'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    less = require('gulp-less');

var path ={
    js: ['web/js/*.js', '!web/js/*.min.*'],
    style: 'web/css/**/*.less'
};

gulp.task('less', function() {
    gulp.src('web/css/*.less')
        .pipe(less())
        .pipe(gulp.dest(function(file) {
            return file.base;
        }));
});

gulp.task('js',function(){
    gulp.src(path.js)
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(function(file) {
            return file.base;
        }));
});

gulp.task('watcher',function(){
    gulp.watch(path.style, ['less']);
    gulp.watch(path.js, ['js']);
});

gulp.task('default', ['watcher']);
<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * AlbumsForm is the model behind the edit form.
 */
class AlbumsForm extends Model
{
    public $id;
    public $onRegataPage;
    public $title;
    public $name;
    public $description;
    public $status;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['title', 'onRegataPage', 'status'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id'            =>  'ID',
            'onRegataPage'  =>  'Отображение на главной странце Регата',
            'title'         =>  'Заголовок',
            'name'          =>  'Название папки',
            'description'   =>  'Описание',
            'status'        =>  'Статус',
        ];
    }
}
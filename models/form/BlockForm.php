<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * BlockForm is the model behind the edit form.
 */
class BlockForm extends Model
{
    public $id;
    public $pageId;
    public $title;
    public $name;
    public $status;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['pageId', 'title', 'status'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id'        =>  'ID',
            'pageId'    =>  'Страница',
            'title'     =>  'Заголовок',
            'name'      =>  'Название',
            'status'    =>  'Статус',
        ];
    }
}
<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $phone;
    public $body;
    public $agreement;
    public $reCaptcha;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject, agreement and body are required
            [['name', 'email', 'subject', 'phone', 'body', 'agreement'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            [
                ['reCaptcha'],
                \himiklab\yii2\recaptcha\ReCaptchaValidator::className(),
                'secret'            =>  '6LcseyAUAAAAADJb--Sdqka8kYPSJ-5dQk483snS',
                'uncheckedMessage'  =>  'Пожалуйста, подтвердите что вы не робот.'
            ]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name'      =>  'Имя',
            'subject'   =>  'Тема',
            'phone'     =>  'Телефон',
            'body'      =>  'Письмо',
            'agreement' =>  'Согласие на обработку персональных данных',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $emails the target email address
     * @return bool whether the model passes validation
     */
    public function contact($emails)
    {
        $email = $emails[0];
        unset($emails[0]);
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setCc($emails)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setHtmlBody('<b>Контактный номер: </b>' . $this->phone . '<br><p>' . $this->body . '</p>')
                ->send();

            return true;
        }
        return false;
    }
}

<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * GalleryImagesForm is the model behind the edit form.
 */
class GalleryImagesForm extends Model
{
    public $id;
    public $albumsId;
    public $urlImage;
    public $alt;
    public $description;
    public $status;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['albumsId', 'urlImage', 'status'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id'            =>  'ID',
            'albumsId'      =>  'Альбом',
            'urlImage'      =>  'Картинка',
            'alt'           =>  'Алтернативный текст (alt)',
            'description'   =>  'Описание',
            'status'        =>  'Статус',
        ];
    }
}
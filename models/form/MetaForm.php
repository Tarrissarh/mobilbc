<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * MetaForm is the model behind the edit form.
 */
class MetaForm extends Model
{
    public $id;
    public $typeId;
    public $type;
    public $name;
    public $value;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['typeId', 'type', 'name', 'value'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id'        =>  'ID',
            'typeId'    =>  'Страница или новость',
            'type'      =>  'Тип',
            'name'      =>  'Название',
            'value'     =>  'Значение',
        ];
    }
}
<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * ObjectForm is the model behind the edit form.
 */
class ObjectForm extends Model
{
    public $id;
    public $square;
    public $floor;
    public $title;
    public $content;
    public $status;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['square', 'floor', 'title', 'content', 'status'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id'        =>  'ID',
            'square'    =>  'Площадь',
            'floor'     =>  'Этаж',
            'title'     =>  'Заголовок',
            'content'   =>  'Контент',
            'status'    =>  'Статус',
        ];
    }
}
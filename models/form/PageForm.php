<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * PageForm is the model behind the edit form.
 */
class PageForm extends Model
{
    public $id;
    public $aliasId;
    public $template;
    public $name;
    public $title;
    public $content;
    public $created;
    public $updated;
    public $status;
    public $canDel;
    public $onMenu;

    public $url;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['aliasId', 'url', 'template', 'title', 'created', 'updated', 'status', 'canDel', 'onMenu'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id'        =>  'ID',
            'aliasId'   =>  'URL адрес',
            'template'  =>  'Шаблон',
            'name'      =>  'Название',
            'title'     =>  'Заголовок',
            'content'   =>  'Контент',
            'created'   =>  'Дата создания',
            'updated'   =>  'Дата редактирования',
            'status'    =>  'Статус',
            'canDel'    =>  'Разрешение на удаление',
            'onMenu'    =>  'Отображение в меню',
        ];
    }
}
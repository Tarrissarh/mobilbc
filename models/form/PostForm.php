<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * PostForm is the model behind the edit form.
 */
class PostForm extends Model
{
    public $id;
    public $aliasId;
    public $title;
    public $template;
    public $summary;
    public $content;
    public $created;
    public $updated;
    public $status;

    public $url;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['aliasId', 'url', 'template', 'title', 'summary', 'content', 'created', 'updated', 'status'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id'        =>  'ID',
            'aliasId'   =>  'URL адрес',
            'url'       =>  'URL адрес',
            'title'     =>  'Заголовок',
            'template'  =>  'Шаблон',
            'summary'   =>  'Краткое описание',
            'content'   =>  'Контент',
            'created'   =>  'Дата создания',
            'updated'   =>  'Дата редактирования',
            'status'    =>  'Статус',
        ];
    }
}
<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * SettingsForm is the model behind the edit form.
 */
class SettingsForm extends Model
{
    public $id;
    public $name;
    public $value;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'value'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id'    =>  'ID',
            'name'  =>  'Название',
            'value' =>  'Значение',
        ];
    }
}
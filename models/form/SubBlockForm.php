<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * SubBlockForm is the model behind the edit form.
 */
class SubBlockForm extends Model
{
    public $id;
    public $blockId;
    public $imageFile;
    public $title;
    public $shortContent;
    public $content;
    public $status;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['blockId', 'title', 'content', 'status'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id'            =>  'ID',
            'blockId'       =>  'Родительский блок',
            'imageFile'     =>  'Картинка',
            'title'         =>  'Заголовок',
            'shortContent'  =>  'Краткое описание',
            'content'       =>  'Контент',
            'status'        =>  'Статус',
        ];
    }
}
<?php

namespace app\models\form;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\table\GalleryImages;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, gif', 'maxFiles' => 100],
        ];
    }

    public function upload($dirUpload, $albumsId)
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $file->saveAs('uploads/' . $dirUpload . $file->name);
                $galleryImages = new GalleryImages();
                $attr                   =   array();
                $attr['urlImage']       =   $dirUpload . $file->name;
                $attr['albumsId']       =   $albumsId;
                $attr['alt']            =   '';
                $attr['description']    =   '';
                $attr['status']         =   1;
                $galleryImages->setAttributes($attr, false);
                $galleryImages->save();
            }
            return true;
        } else {
            return false;
        }
    }
}
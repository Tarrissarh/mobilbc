<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class Albums extends ActiveRecord
{
    public static function tableName()
    {
        return 'albums';
    }

    public function rules() {
        return [
            [['onRegataPage', 'title', 'status'], 'required'],
            [['name', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'            =>  'ID',
            'onRegataPage'  =>  'Отображение на главной странце Регата',
            'title'         =>  'Заголовок',
            'name'          =>  'Название папки',
            'description'   =>  'Описание',
            'status'        =>  'Статус',
        );
    }
}
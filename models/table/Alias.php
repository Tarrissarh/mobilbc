<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class Alias extends ActiveRecord
{
    public static function tableName()
    {
        return 'alias';
    }

    public function rules() {
        return [
            [['url'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'    =>  'ID',
            'url'   =>  'URL адрес',
        );
    }
}
<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class Block extends ActiveRecord
{
    public static function tableName()
    {
        return 'block';
    }

    public function rules() {
        return [
            [['pageId', 'title', 'status'], 'required'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'        =>  'ID',
            'pageId'    =>  'Страница',
            'title'     =>  'Заголовок',
            'name'      =>  'Название',
            'status'    =>  'Статус',
        );
    }
}
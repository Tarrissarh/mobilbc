<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class FeedBack extends ActiveRecord
{
    public static function tableName()
    {
        return 'feedback';
    }

    public function rules() {
        return [
            [['to', 'from', 'name', 'phone', 'subject', 'body', 'created'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'        =>  'ID',
            'to'        =>  'Кому',
            'from'      =>  'От кого',
            'name'      =>  'Имя',
            'phone'     =>  'Телефон',
            'subject'   =>  'Тема',
            'body'      =>  'Содержимое',
            'created'   =>  'Дата',
        );
    }
}
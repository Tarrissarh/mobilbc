<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class GalleryImages extends ActiveRecord
{
    public static function tableName()
    {
        return 'gallery_images';
    }

    public function rules() {
        return [
            [['albumsId', 'urlImage', 'status'], 'required'],
            [['alt', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'            =>  'ID',
            'albumsId'      =>  'Альбом',
            'urlImage'      =>  'Картинка',
            'alt'           =>  'Алтернативный текст (alt)',
            'description'   =>  'Описание',
            'status'        =>  'Статус',
        );
    }
}
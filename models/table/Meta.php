<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class Meta extends ActiveRecord
{
    public static function tableName()
    {
        return 'meta';
    }

    public function rules() {
        return [
            [['typeId', 'type', 'name', 'value'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'        =>  'ID',
            'typeId'    =>  'Страница или новость',
            'type'      =>  'Тип',
            'name'      =>  'Название',
            'value'     =>  'Значение',
        );
    }
}
<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class Object extends ActiveRecord
{
    public static function tableName()
    {
        return 'object';
    }

    public function rules() {
        return [
            [['square', 'floor', 'title', 'content', 'status'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'        =>  'ID',
            'square'    =>  'Площадь',
            'floor'     =>  'Этаж',
            'title'     =>  'Заголовок',
            'content'   =>  'Контент',
            'status'    =>  'Статус',
        );
    }
}
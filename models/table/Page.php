<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class Page extends ActiveRecord
{
    public static function tableName()
    {
        return 'page';
    }

    public function rules() {
        return [
            [['aliasId', 'template', 'title', 'created', 'updated', 'status', 'canDel', 'onMenu'], 'required'],
            [['name', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'        =>  'ID',
            'aliasId'   =>  'URL адрес',
            'template'  =>  'Шаблон',
            'name'      =>  'Название',
            'title'     =>  'Заголовок',
            'content'   =>  'Контент',
            'created'   =>  'Дата создания',
            'updated'   =>  'Дата редактирования',
            'status'    =>  'Статус',
            'canDel'    =>  'Разрешение на удаление',
            'onMenu'    =>  'Отображение в меню',
        );
    }
}
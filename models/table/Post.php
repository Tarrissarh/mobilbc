<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class Post extends ActiveRecord
{
    public static function tableName()
    {
        return 'post';
    }

    public function rules() {
        return [
            [['aliasId', 'template', 'title', 'summary', 'content', 'created', 'updated', 'status'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'        =>  'ID',
            'aliasId'   =>  'URL адрес',
            'title'     =>  'Заголовок',
            'template'  =>  'Шаблон',
            'summary'   =>  'Краткое описание',
            'content'   =>  'Контент',
            'created'   =>  'Дата создания',
            'updated'   =>  'Дата редактирования',
            'status'    =>  'Статус',
        );
    }
}
<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class Settings extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings';
    }

    public function rules() {
        return [
            [['name', 'value'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'    =>  'ID',
            'name'  =>  'Название',
            'value' =>  'Значение',
        );
    }
}
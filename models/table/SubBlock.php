<?php

namespace app\models\table;

use yii\db\ActiveRecord;

class SubBlock extends ActiveRecord
{
    public static function tableName()
    {
        return 'sub_block';
    }

    public function rules() {
        return [
            [['blockId', 'title', 'content', 'status'], 'required'],
            [['imageFile', 'shortContent'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array(
            'id'            =>  'ID',
            'blockId'       =>  'Родительский блок',
            'imageFile'     =>  'Картинка',
            'title'         =>  'Заголовок',
            'shortContent'  =>  'Краткое описание',
            'content'       =>  'Контент',
            'status'        =>  'Статус',
        );
    }
}
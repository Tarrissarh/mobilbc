<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dosamigos\datetimepicker\DateTimePicker;
use dosamigos\tinymce\TinyMce;
use yii\helpers\FileHelper;

use app\models\table\Albums;

$this->registerJsFile(
    '@web/js/admin/ajax.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$status = [
    0           =>  'Выключено',
    1           =>  'Включено',
    'publish'   =>  'Опубликовано',
    'draft'     =>  'Закрыто к публикации',
];

$form = ActiveForm::begin([
    'action'    =>  '/admin/save/' . $type . '/',
    'id'        =>  'create' . ucfirst($type),
    'method'    =>  'post',
    'options'   => [
        'enctype'   =>  'multipart/form-data',
        'class'     =>  'form-horizontal'
    ],
]);

if (!is_null($labels)):
    echo $form->field($model, 'saveTemplate')->hiddenInput(['value' => 'create'])->label(false);
    if ($type == 'gi') {
        echo $form->field($model, 'albumsId')->hiddenInput(['value' => $albumsId])->label(false);
    } elseif ($type == 'page') {
        echo $form->field($model, 'canDel')->hiddenInput(['value' => 1])->label(false);
    }

    foreach ($labels as $label => $value) {
        if ($label == 'id') {
            echo $form->field($model, $label)->hiddenInput()->label(false);
        } elseif ($label == 'onMenu') {
            echo $form->field($model, $label)->checkbox();
        } elseif ($label == 'pageId') {
            $items[0] = 'Общая';
            foreach ($data['pages'] as $key => $datum) {
                $items[$datum->attributes['id']] = $datum->attributes['title'];
            }

            echo $form->field($model, $label)->dropDownList($items, [
                'prompt' => 'Выберите страницу'
            ]);
        } elseif ($label == 'blockId') {
            foreach ($data['blocks'] as $key => $datum) {
                $items[$datum->attributes['id']] = $datum->attributes['title'];
            }

            echo $form->field($model, $label)->dropDownList($items, [
                'prompt' => 'Выберите блок'
            ]);
            ?>
            <div id="imageFileBlock">
                <?php
                $this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.css');
                $this->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
                $this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.js', [
                    'depends' => 'yii\web\JqueryAsset',
                    'position' => \yii\web\View::POS_HEAD
                ]);

                echo $form->field($model, 'imageFile', [
                    'template' => "{label}\n
                    <a href=\"/plugins/tinymce/responsivefilemanager/filemanager/dialog.php?type=1&amp;field_id=imageFile&amp;relative_url=1\" class=\"btn iframe-btn\" type=\"button\">
                        <i class=\"fa fa-upload\" aria-hidden=\"true\"></i>
                    </a>
                    <a type='button' id='removeImageFile' class=\"btn btn-link\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>
                    {input}\n
                    {hint}\n
                    {error}"
                ])->textInput([
                    'id'            =>  'imageFile',
                    'placeholder'   =>  'Загрузите изображение',
                    'readonly'      =>  true
                ]);
                $this->registerJsFile('@web/js/admin/iframe.js', ['depends' => 'yii\web\JqueryAsset']);
                ?>
            </div>
            <?php
        } elseif ($label == 'title' || $label == 'alt') {
            echo $form->field($model, $label)->textInput();
        } elseif ($label == 'name' && $type != 'settings' && $type != 'meta') {
            echo $form->field($model, $label)->hiddenInput(['value' => ''])->label(false);
        } elseif ($label == 'name' && ($type == 'settings' || $type == 'meta')) {
            echo $form->field($model, $label)->textInput();
        } elseif ($label == 'floor') {
            echo $form->field($model, $label)->textInput(['value' => 1]);
        } elseif ($label == 'square') {
            echo $form->field($model, $label)->textInput([
                'value'         =>  '00000.00',
                'placeholder'   =>  '00000.00'
            ]);
        } elseif ($label == 'template') {
            $files = FileHelper::findFiles(FileHelper::normalizePath('../views/site/' . $type . '_template'), [
                'only' => ['*.php'],
                'recursive' => true]
            );

            foreach ($files as $key => $file) {
                $files[$key] = explode('.', explode('\\', $file)[4])[0];
            }

            foreach ($files as $k => $file) {
                $dropDownList[$file] = $file;
            }

            echo $form->field($model, $label)->dropDownList($dropDownList, [
                'options' => ['main' => ['selected' => true]]
            ]);
        } elseif ($label == 'aliasId') {
            echo $form->field($model, 'url')->textInput()->label($value);
            echo $form->field($model, 'aliasId')->hiddenInput(['value' => 0])->label(false);
        } elseif ($label == 'content' || $label == 'summary') {
            echo $form->field($model, $label)->widget(TinyMce::className(), [
                'options' => [
                    'rows' => 6,
                ],
                'language' => 'ru',
                'clientOptions' => [
                    'plugins' => [
                        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                        'searchreplace wordcount visualblocks visualchars code fullscreen',
                        'insertdatetime media nonbreaking save table contextmenu directionality',
                        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc responsivefilemanager filemanager',
                    ],
                    'toolbar1' => 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    'toolbar2' => 'print preview media | forecolor backcolor emoticons | codesample | responsivefilemanager | restoredraft',
                    'image_advtab' => true,
                    'external_filemanager_path' => "/plugins/tinymce/responsivefilemanager/filemanager/",
                    'filemanager_title' => "Responsive Filemanager" ,
                    'external_plugins' => [
                        'filemanager' => "/plugins/tinymce/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js",
                        'responsivefilemanager' => '/plugins/tinymce/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                    ],
                    'autosave_interval' => "10s"
                ]
            ]);
        } elseif ($label == 'shortContent') {
            if ($model->attributes['blockId'] == 2) {
                echo $form->field($model, $label)->widget(TinyMce::className(), [
                    'options' => [
                        'rows' => 6,
                        'value' => $model->attributes[$label]
                    ],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                            'searchreplace wordcount visualblocks visualchars code fullscreen',
                            'insertdatetime media nonbreaking save table contextmenu directionality',
                            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc responsivefilemanager filemanager',
                        ],
                        'toolbar1' => 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                        'toolbar2' => 'print preview media | forecolor backcolor emoticons | codesample | responsivefilemanager | restoredraft',
                        'image_advtab' => true,
                        'external_filemanager_path' => "/plugins/tinymce/responsivefilemanager/filemanager/",
                        'filemanager_title' => "Responsive Filemanager" ,
                        'external_plugins' => [
                            'filemanager' => "/plugins/tinymce/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js",
                            'responsivefilemanager' => '/plugins/tinymce/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                        ],
                        'autosave_interval' => "10s"
                    ]
                ]);
            } else {
                echo $form->field($model, $label)->hiddenInput(['value' => ''])->label(false);
            }
        } elseif ($label == 'created') {
            echo $form->field($model, $label)->widget(DateTimePicker::className(), [
                'language' => 'ru',
                'size' => 'ms',
                'clientOptions' => [
                    'startView' => 1,
                    'minView' => 0,
                    'maxView' => 1,
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii:ss',
                    'todayBtn' => true
                ]
            ]);
        } elseif ($label == 'updated') {
            echo $form->field($model, $label)->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false);
        } elseif ($label == 'status') {
            if (is_null($model->attributes['status'])) {
                $radioList = [
                    1 => $status[1],
                    0 => $status[0]
                ];

                $model->$label = 1;

                echo $form->field($model, $label)->radioList($radioList);
            } else {
                $dropDownList = [
                    'publish' => $status['publish'],
                    'draft' => $status['draft']
                ];

                echo $form->field($model, $label)->dropDownList($dropDownList, [
                    'options'   =>  ['publish' => ['selected' => true]]
                ]);
            }
        } elseif ($label == 'onRegataPage') {
            $albumsOnRegataPage = Albums::find()->where([
                'onRegataPage'  =>  1,
                'status'        =>  1
            ])->one();

            if (is_null($albumsOnRegataPage) && empty($albumsOnRegataPage)) {
                $disable = false;
            } else {
                $disable = true;
            }

            $radioList = [
                1 => $status[1],
                0 => $status[0]
            ];

            $model->$label = 0;

            echo $form->field($model, $label)->radioList($radioList, ['itemOptions' => ['disabled' => $disable]]);
        } elseif ($label == 'value' || $label == 'description') {
            echo $form->field($model, $label)->textarea();
        } elseif ($label == 'typeId') {
            foreach ($data['pages'] as $key => $datum) {
                $items[$datum->attributes['id']] = $datum->attributes['title'];
            }

            echo $form->field($model, $label)->dropDownList($items, [
                'prompt'    =>  'Выберите страницу',
                'id'        =>  'metaTypeId',
            ]);
        } elseif ($label == 'type') {
            echo $form->field($model, $label)->dropDownList([
                'page' => 'Страница',
                'post' => 'Новость'
            ], [
                'id'        =>  'metaType',
                'options'   =>  ['page' => ['selected' => true]]
            ]);
        } elseif ($label == 'urlImage') {
            ?>
            <div id="galleryImages">
                <?php
                $this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.css');
                $this->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
                $this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.js', [
                    'depends' => 'yii\web\JqueryAsset',
                    'position' => \yii\web\View::POS_HEAD
                ]);

                echo $form->field($model, 'urlImage', [
                    'template' => "{label}\n
                        <a href=\"/plugins/tinymce/responsivefilemanager/filemanager/dialog.php?type=1&amp;field_id=urlImage&amp;relative_url=1\" class=\"btn iframe-btn\" type=\"button\">
                            <i class=\"fa fa-upload\" aria-hidden=\"true\"></i>
                        </a>
                        <a type='button' id='removeUrl' class=\"btn btn-link\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>
                        {input}\n
                        {hint}\n
                        {error}"
                ])->textInput([
                    'id'            =>  'urlImage',
                    'placeholder'   =>  'Загрузите изображение',
                    'readonly'      =>  true
                ]);
                $this->registerJsFile('@web/js/admin/iframe.js', ['depends' => 'yii\web\JqueryAsset']);
                ?>
            </div>
            <?php
        }
    }
endif;

echo Html::submitButton('Создать', [
    'class' => 'btn btn-primary',
    'id' => 'create',
]);
echo Html::a('Назад', Yii::$app->homeUrl . 'admin/', ['class' => 'btn btn-primary']);

ActiveForm::end();

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dosamigos\datetimepicker\DateTimePicker;
use dosamigos\tinymce\TinyMce;
use yii\helpers\FileHelper;

use app\models\table\Albums;

$this->registerJsFile(
    '@web/js/admin/ajax.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile('@web/js/admin/edit.js', ['depends' => 'yii\web\JqueryAsset']);

$status = [
    0           =>  'Выключено',
    1           =>  'Включено',
    'publish'   =>  'Опубликовано',
    'draft'     =>  'Закрыто к публикации',
];

$form = ActiveForm::begin([
    'action'    =>  '/admin/save/' . $type . '/',
    'id'        =>  'edit' . ucfirst($type),
    'method'    =>  'post',
    'options'   => [
        'enctype'   =>  'multipart/form-data',
        'class'     =>  'form-horizontal'
    ],
]);

if (!is_null($labels)):
    echo $form->field($model, 'saveTemplate')->hiddenInput(['value' => 'edit'])->label(false);

    if ($type == 'gi') {
        echo $form->field($model, 'albumsId')->hiddenInput(['value' => $model->attributes['albumsId']])->label(false);
    }

    foreach ($labels as $label => $value) {
        if ($label == 'id') {
            echo $form->field($model, $label)->hiddenInput(['value' => $model->attributes[$label]])->label(false);
        } elseif ($label == 'pageId') {
            $items[0] = 'Общая';
            $active = 0;
            foreach ($data['pages'] as $key => $datum) {
                if ($datum->attributes['id'] == $model->attributes[$label]) {
                    $active = $datum->attributes['id'];
                }
                $items[$datum->attributes['id']] = $datum->attributes['title'];
            }

            echo $form->field($model, $label)->dropDownList($items, [
                'prompt' => 'Выберите страницу',
                'options' => [$active => ['selected' => true]]
            ]);
        } elseif ($label == 'blockId') {
            foreach ($data['blocks'] as $key => $datum) {
                if ($datum->attributes['id'] == $model->attributes[$label]) {
                    $active = $datum->attributes['id'];
                }
                $items[$datum->attributes['id']] = $datum->attributes['title'];
            }

            echo $form->field($model, $label)->dropDownList($items, [
                'prompt'    =>  'Выберите блок',
                'options'   =>  [$active =>  ['selected' => true]],
                'id'        =>  'blocks'
            ]);
            ?>
            <div id="imageFileBlock">
                <?php
                $this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.css');
                $this->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
                $this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.js', [
                    'depends' => 'yii\web\JqueryAsset',
                    'position' => \yii\web\View::POS_HEAD
                ]);

                echo $form->field($model, 'imageFile', [
                    'template' => "{label}\n
                    <a href=\"/plugins/tinymce/responsivefilemanager/filemanager/dialog.php?type=1&amp;field_id=imageFile&amp;relative_url=1\" class=\"btn iframe-btn\" type=\"button\">
                        <i class=\"fa fa-upload\" aria-hidden=\"true\"></i>
                    </a>
                    <a type='button' id='removeImageFile' class=\"btn btn-link\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>
                    {input}\n
                    {hint}\n
                    {error}"
                ])->textInput([
                    'value'         =>  $model->attributes['imageFile'],
                    'id'            =>  'imageFile',
                    'placeholder'   =>  'Загрузите изображение',
                    'readonly'      =>  true
                ]);
                $this->registerJsFile('@web/js/admin/iframe.js', ['depends' => 'yii\web\JqueryAsset']);
                ?>
            </div>
            <?php
        } elseif ($label == 'title' || $label == 'alt') {
            echo $form->field($model, $label)->textInput(['value' => $model->attributes[$label]]);
        } elseif ($label == 'name' && $type != 'settings' && $type != 'meta') {
            echo $form->field($model, $label)->hiddenInput(['value' => $model->attributes[$label]])->label(false);
        } elseif ($label == 'name' && ($type == 'settings' || $type == 'meta')) {
            echo $form->field($model, $label)->textInput(['value' => $model->attributes[$label]]);
        } elseif ($label == 'floor') {
            echo $form->field($model, $label)->textInput(['value' => $model->attributes[$label]]);
        } elseif ($label == 'square') {
            echo $form->field($model, $label)->textInput([
                'value'         => $model->attributes[$label],
                'placeholder'   =>  '00000.00'
            ]);
        } elseif ($label == 'aliasId') {
            $alias = $data['alias'];
            foreach ($alias as $url) {
                if ($model->attributes[$label] == $url->attributes['id']) {
                    echo $form->field($model, 'url')->textInput(['value' => $url->attributes['url']])->label($value);
                    echo $form->field($model, 'aliasId')->hiddenInput(['value' => $model->attributes[$label]])->label(false);
                }
            }
        } elseif ($label == 'template') {
            $files = FileHelper::findFiles(FileHelper::normalizePath('../views/site/' . $type . '_template'), ['only' => ['*.php'], 'recursive' => true]);
            foreach ($files as $key => $file) {
                $files[$key] = explode('.', explode('\\', $file)[4])[0];
            }

            foreach ($files as $file) {
                if ($model->attributes[$label] == $file) {
                    $active = $file;
                }
                $dropDownList[$file] = $file;
            }

            echo $form->field($model, $label)->dropDownList($dropDownList, [
                'options' => [$active => ['selected' => true]]
            ]);
        } elseif ($label == 'content' || $label == 'summary') {
            echo $form->field($model, $label)->widget(TinyMce::className(), [
                'options' => [
                    'rows' => 6,
                    'value' => $model->attributes[$label]
                ],
                'language' => 'ru',
                'clientOptions' => [
                    'plugins' => [
                        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                        'searchreplace wordcount visualblocks visualchars code fullscreen',
                        'insertdatetime media nonbreaking save table contextmenu directionality',
                        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc responsivefilemanager filemanager',
                    ],
                    'toolbar1' => 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    'toolbar2' => 'print preview media | forecolor backcolor emoticons | codesample | responsivefilemanager | restoredraft',
                    'image_advtab' => true,
                    'external_filemanager_path' => "/plugins/tinymce/responsivefilemanager/filemanager/",
                    'filemanager_title' => "Responsive Filemanager" ,
                    'external_plugins' => [
                        'filemanager' => "/plugins/tinymce/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js",
                        'responsivefilemanager' => '/plugins/tinymce/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                    ],
                    'autosave_interval' => "10s"
                ]
            ]);
        } elseif ($label == 'shortContent') {
            if ($model->attributes['blockId'] == 2) {
                echo $form->field($model, $label)->widget(TinyMce::className(), [
                    'options' => [
                        'rows' => 6,
                        'value' => $model->attributes[$label]
                    ],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                            'searchreplace wordcount visualblocks visualchars code fullscreen',
                            'insertdatetime media nonbreaking save table contextmenu directionality',
                            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc responsivefilemanager filemanager',
                        ],
                        'toolbar1' => 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                        'toolbar2' => 'print preview media | forecolor backcolor emoticons | codesample | responsivefilemanager | restoredraft',
                        'image_advtab' => true,
                        'external_filemanager_path' => "/plugins/tinymce/responsivefilemanager/filemanager/",
                        'filemanager_title' => "Responsive Filemanager" ,
                        'external_plugins' => [
                            'filemanager' => "/plugins/tinymce/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js",
                            'responsivefilemanager' => '/plugins/tinymce/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                        ],
                        'autosave_interval' => "10s"
                    ]
                ]);
            } else {
                echo $form->field($model, $label)->hiddenInput(['value' => ''])->label(false);
            }
        } elseif ($label == 'created') {
            echo $form->field($model, $label)->widget(DateTimePicker::className(), [
                'language' => 'ru',
                'size' => 'ms',
                'value' => $model->attributes[$label],
                'clientOptions' => [
                    'startView' => 1,
                    'minView' => 0,
                    'maxView' => 1,
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii:ss',
                    'todayBtn' => true
                ]
            ]);
        } elseif ($label == 'updated') {
            echo $form->field($model, $label)->hiddenInput(['value' => date('Y-m-d H:i:s')])->label(false);
        } elseif ($label == 'canDel') {
            echo $form->field($model, 'canDel')->hiddenInput(['value' => $model->attributes[$label]])->label(false);
        } elseif ($label == 'onMenu') {
            echo $form->field($model, $label)->checkbox();
        } elseif ($label == 'status') {
            if (is_int($model->attributes['status'])) {
                if ($model->attributes['status'] == 1) {
                    $model->$label = 1;
                } else {
                    $model->$label = 0;
                }

                echo $form->field($model, $label)->radioList([
                    1 => $status[1],
                    0 => $status[0]
                ]);
            } else {
                if ($model->attributes['status'] == 'publish') {
                    $active = 'publish';
                } else {
                    $active = 'draft';
                }

                echo $form->field($model, $label)->dropDownList([
                    'publish' => $status['publish'],
                    'draft' => $status['draft']
                ], [
                    'options' => [$active => ['selected' => true]]
                ]);
            }
        } elseif ($label == 'onRegataPage') {
            $albumsOnRegataPage = Albums::find()->where([
                'onRegataPage'  =>  1,
                'status'        =>  1
            ])->one();

            if (is_null($albumsOnRegataPage) && empty($albumsOnRegataPage)) {
                $disable = false;
            } else {
                if ($model->attributes['id'] == $albumsOnRegataPage->attributes['id']) {
                    $disable = false;
                } else {
                    $disable = true;
                }
            }

            if ($model->attributes['onRegataPage'] == 1) {
                $model->$label = 1;
            } else {
                $model->$label = 0;
            }

            echo $form->field($model, $label)->radioList([
                1 => $status[1],
                0 => $status[0]
            ], ['itemOptions' => ['disabled' => $disable]]);
        } elseif ($label == 'value' || $label == 'description') {
            echo $form->field($model, $label)->textarea(['value' => $model->attributes[$label]]);
        } elseif ($label == 'typeId') {
            $active = 0;
            if ($model->attributes['type'] == 'post') {
                foreach ($data['posts'] as $key => $datum) {
                    if ($datum->attributes['id'] == $model->attributes[$label]) {
                        $active = $datum->attributes['id'];
                    }
                    $items[$datum->attributes['id']] = $datum->attributes['title'];
                }
                $text = 'новость';
            } else {
                foreach ($data['pages'] as $key => $datum) {
                    if ($datum->attributes['id'] == $model->attributes[$label]) {
                        $active = $datum->attributes['id'];
                    }
                    $items[$datum->attributes['id']] = $datum->attributes['title'];
                }
                $text = 'страницу';
            }

            echo $form->field($model, $label)->dropDownList($items, [
                'prompt'    =>  'Выберите ' . $text,
                'id'        =>  'metaTypeId',
                'options'   =>  [$active => ['selected' => true]]
            ]);
        } elseif ($label == 'type') {
            if ($model->attributes[$label] == 'post') {
                $active = 'post';
            } else {
                $active = 'page';
            }

            echo $form->field($model, $label)->dropDownList([
                'page' => 'Страница',
                'post' => 'Новость'
            ], [
                'id'        =>  'metaType',
                'options'   =>  [$active => ['selected' => true]]
            ]);
        } elseif ($label == 'urlImage') {
            ?>
                <div id="galleryImages">
                    <?php
                    $this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.css');
                    $this->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
                    $this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.js', [
                        'depends' => 'yii\web\JqueryAsset',
                        'position' => \yii\web\View::POS_HEAD
                    ]);

                    echo $form->field($model, 'urlImage', [
                        'template' => "{label}\n
                        <a href=\"/plugins/tinymce/responsivefilemanager/filemanager/dialog.php?type=1&amp;field_id=urlImage&amp;relative_url=1\" class=\"btn iframe-btn\" type=\"button\">
                            <i class=\"fa fa-upload\" aria-hidden=\"true\"></i>
                        </a>
                        <a type='button' id='removeUrl' class=\"btn btn-link\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>
                        {input}\n
                        {hint}\n
                        {error}"
                    ])->textInput([
                        'value'         =>  $model->attributes['urlImage'],
                        'id'            =>  'urlImage',
                        'placeholder'   =>  'Загрузите изображение',
                        'readonly'      =>  true
                    ]);
                    $this->registerJsFile('@web/js/admin/iframe.js', ['depends' => 'yii\web\JqueryAsset']);
                    ?>
                </div>
            <?php
        }
    }
endif;

echo Html::submitButton('Сохранить', [
    'class' => 'btn btn-primary',
    'id' => 'create',
]);
echo Html::a('Назад', Yii::$app->homeUrl . 'admin/', ['class' => 'btn btn-primary']);

if ($type == 'page') {
    if ((int)$model->attributes['canDel'] != 0) {
        echo Html::a('Удалить', '/admin/delete/' . $type . '/' . $model->attributes['id'] . '/', [
            'class' => 'btn btn-danger',
            'id' => 'delete',
        ]);
    }
} else {
    echo Html::a('Удалить', '/admin/delete/' . $type . '/' . $model->attributes['id'] . '/', [
        'class' => 'btn btn-danger',
        'id' => 'delete',
    ]);
}

ActiveForm::end();

?>

<?php
if ($type == 'albums'):
    $galleryImages          =   $data['galleryImages'];
    $galleryImagesLabels    =   $data['galleryImagesLabels'];
?>

    <?php if (!is_null($galleryImages) && !empty($galleryImages)):
        $this->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        $this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.css');
        $this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.js', [
            'depends' => 'yii\web\JqueryAsset',
            'position' => \yii\web\View::POS_HEAD
        ]);
        $this->registerJsFile('@web/js/admin/iframe.js', ['depends' => 'yii\web\JqueryAsset']);
        ?>
        <div id="blocks_gallery_images">
            <h1>Картинки</h1>
            <?=Html::a(
                'Добавить картинку',
                ['javascript:;'],
                ['class' => 'btn btn-primary', 'data-fancybox_custom' => '', 'data-src' => '/admin/create/gi/' . $albumsId . '/']
            );?>
            <?=Html::a(
                'Добавить несколько картинок',
                ['javascript:;'],
                ['class' => 'btn btn-primary', 'data-fancybox_custom' => '', 'data-src' => '/admin/upload/' . $albumsId . '/']
            );?>
            <div class="block">
                <?php foreach ($galleryImages as $galleryImage): ?>
                <?php if($galleryImage->attributes['albumsId'] == $albumsId): ?>
                    <div class="item_gallery item_gallery_id_<?=$galleryImage->attributes['id'];?>">
                        <div class="img_block">
                            <a href="<?=Url::to('@web/uploads/') . $galleryImage->attributes['urlImage'];?>" data-fancybox="images">
                                <img src="<?=Url::to('@web/uploads/') . $galleryImage->attributes['urlImage'];?>" >
                            </a>
                            <div class="buttons">
                                <?=Html::a(
                                    '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>',
                                    ['javascript:;'],
                                    ['class' => 'button button_edit', 'data-fancybox_custom' => '', 'data-src' => '/admin/edit/gi/' . $galleryImage->attributes['id']]
                                );
                                echo Html::a(
                                    '<i class="fa fa-times" aria-hidden="true"></i>',
                                    '/admin/delete/gi/' . $galleryImage->attributes['id'] . '/',
                                    ['class' => 'button button_delete']
                                );?>
                            </div>
                        </div>
                        <p>Альтернативный текст (alt) картинки: <?=$galleryImage->attributes['alt'];?></p>
                        <p>Описание картинки: <?=$galleryImage->attributes['description'];?></p>
                        <p>Статус картинки: <?=$status[$galleryImage->attributes['status']];?></p>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            </div>
        </div>
    <?php else: ?>
        <div class="col-lg-12 text-center">
            <h1 class="main">Картинок нет</h1>
            <?=Html::a(
                'Добавить картинку',
                ['admin/create/gi/' . $albumsId . '/'],
                ['class' => 'btn btn-primary']
            );?>
        </div>
    <?php endif; ?>
<?php endif; ?>
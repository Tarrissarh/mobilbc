<?php

use yii\helpers\Html;
use app\models\table\SubBlock;
use app\models\table\Meta;

$status = [
    0 => 'Выключено',
    1 => 'Включено',
    'publish' => 'Опубликовано',
    'draft' => 'Закрыто к публикации',
];

?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="text-center">Панель администратора</h1>
        <div class="panel panel-default">
            <ul class="nav nav-pills panel-heading">
                <li class="active"><a data-toggle="tab" href="#index">Главная</a></li>
                <li><a data-toggle="tab" href="#regata">Регата</a></li>
                <li><a data-toggle="tab" href="#news">Новости</a></li>
                <li><a data-toggle="tab" href="#gallery">Фотоотчеты</a></li>
                <li><a data-toggle="tab" href="#pages">Страницы</a></li>
                <li><a data-toggle="tab" href="#settings">Настройки</a></li>
                <li><a data-toggle="tab" href="#feedback">Обратная связь</a></li>
                <li><a data-toggle="tab" href="#objects">Объекты</a></li>
            </ul>
            <div class="tab-content panel-body">
                <div id="index" class="tab-pane fade in active">
                    <h1><?=$mainPage->attributes['title'];?></h1>
                    <?=Html::a(
                        'Редактировать',
                        ['admin/edit/page/' . $mainPage->attributes['id']],
                        ['class' => 'btn btn-primary']
                    );?>
                    <div class="block">
                        <h2>Мета информация</h2>
                        <?php
                        $metas = Meta::find()->where([
                            'typeId'    =>  $mainPage->attributes['id'],
                            'type'      =>  'page'
                        ])->all();
                        ?>
                        <?php if (!is_null($metas) && !empty($metas)): ?>
                            <?php
                            $metaClass  =   new Meta();
                            $metaLabels =   $metaClass->attributeLabels();
                            ?>
                            <div class="block_btn">
                                <?=Html::a(
                                    'Добавить мета тег',
                                    ['admin/create/meta/'],
                                    ['class' => 'btn btn-primary']
                                );?>
                            </div>
                            <?php foreach ($metas as $meta):?>
                                <div class="item_meta">
                                    <h4>Тип: <?=$meta->attributes['name'];?></h4>
                                    <p>Содержание: <?=$meta->attributes['value'];?></p>
                                    <?=Html::a('Редактировать', ['/admin/edit/meta/' . $meta->attributes['id']]);?>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="item_meta">
                                <p>Мета тегов нет</p>
                                <?=Html::a(
                                    'Добавить мета тег',
                                    ['admin/create/meta/'],
                                    ['class' => 'btn btn-primary']
                                );?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if (!is_null($blockMain)): ?>
                        <?php foreach ($blockMain as $block): ?>
                            <?php
                            $subBlocks = SubBlock::find()->where(['blockId' => $block->attributes['id']])->all();
                            ?>
                            <?php if (!is_null($subBlocks)): ?>
                                <?php
                                $subBlockClass  =   new SubBlock();
                                $subBlockLabels =   $subBlockClass->attributeLabels();
                                ?>
                                <div class="block">
                                    <h2>Блок: <?=$block->attributes['title'];?></h2>
                                    <div class="block_btn">
                                        <?=Html::a(
                                            'Редактировать',
                                            ['admin/edit/block/' . $block->attributes['id']],
                                            ['class' => 'btn btn-primary']
                                        );?>
                                        <?=Html::a(
                                            'Добавить подблок',
                                            ['admin/create/sub_block/'],
                                            ['class' => 'btn btn-primary']
                                        );?>
                                    </div>
                                    <?php foreach ($subBlocks as $k => $subBlock): ?>
                                        <?php if ($subBlock->attributes['blockId'] == $block->attributes['id']): ?>
                                            <div class="block_sup">
                                                <h3><?=$subBlock->attributes['title'];?></h3>
                                                <?=Html::a(
                                                    'Редактировать',
                                                    ['admin/edit/sub_block/' . $subBlock->attributes['id']],
                                                    ['class' => 'btn btn-primary']
                                                );?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php else: ?>
                                <div>Подблоков нет</div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div>Блоков нет</div>
                    <?php endif; ?>
                    <?php if (!is_null($blockCommon)): ?>
                        <?php foreach ($blockCommon as $block): ?>
                            <?php
                            $subBlocks = SubBlock::find()->where(['blockId' => $block->attributes['id']])->all();
                            ?>
                            <?php if (!is_null($subBlocks)): ?>
                                <?php
                                $subBlockClass  =   new SubBlock();
                                $subBlockLabels =   $subBlockClass->attributeLabels();
                                ?>
                                <div class="block">
                                    <h2>Блок: <?=$block->attributes['title'];?> (Общий)</h2>
                                    <div>
                                        <?=Html::a(
                                            'Редактировать',
                                            ['admin/edit/block/' . $block->attributes['id']],
                                            ['class' => 'btn btn-primary']
                                        );?>
                                        <?=Html::a(
                                            'Добавить подблок',
                                            ['admin/create/sub_block/'],
                                            ['class' => 'btn btn-primary']
                                        );?>
                                    </div>
                                    <?php foreach ($subBlocks as $k => $subBlock): ?>
                                        <?php if ($subBlock->attributes['blockId'] == $block->attributes['id']): ?>
                                            <div class="block_sup">
                                                <h3><?=$subBlock->attributes['title'];?></h3>
                                                <?=Html::a(
                                                    'Редактировать',
                                                    ['admin/edit/sub_block/' . $subBlock->attributes['id']],
                                                    ['class' => 'btn btn-primary']
                                                );?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php else: ?>
                                <div>Подблоков нет</div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div>Блоков нет</div>
                    <?php endif; ?>
                </div>
                <div id="regata" class="tab-pane fade">
                    <h1><?=$regataPage->attributes['title'];?></h1>
                    <?=Html::a(
                        'Редактировать',
                        ['admin/edit/page/' . $regataPage->attributes['id']],
                        ['class' => 'btn btn-primary']
                    );?>
                    <div class="block">
                        <h2>Мета информация</h2>
                        <?php
                        $metas = Meta::find()->where([
                            'typeId'    =>  $regataPage->attributes['id'],
                            'type'      =>  'page'
                        ])->all();
                        ?>
                        <?php if (!is_null($metas) && !empty($metas)): ?>
                            <?php
                            $metaClass  =   new Meta();
                            $metaLabels =   $metaClass->attributeLabels();
                            ?>
                            <div class="block_btn">
                                <?=Html::a(
                                    'Добавить мета тег',
                                    ['admin/create/meta/'],
                                    ['class' => 'btn btn-primary']
                                );?>
                            </div>
                            <?php foreach ($metas as $meta):?>
                                <div class="item_meta">
                                    <h4>Тип: <?=$meta->attributes['name'];?></h4>
                                    <p>Содержание: <?=$meta->attributes['value'];?></p>
                                    <?=Html::a('Редактировать', ['/admin/edit/meta/' . $meta->attributes['id']]);?>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="block_btn">
                                <?=Html::a(
                                    'Добавить мета тег',
                                    ['admin/create/meta/'],
                                    ['class' => 'btn btn-primary']
                                );?>
                            </div>
                            <div class="item_meta">
                                <p>Мета тегов нет</p>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if (!is_null($blockRegata)): ?>
                        <?php foreach ($blockRegata as $key => $block): ?>
                            <?php
                            $subBlocks = SubBlock::find()->where(['blockId' => $block->attributes['id']])->all();
                            ?>
                            <?php if (!is_null($subBlocks)): ?>
                                <?php
                                $subBlockClass  =   new SubBlock();
                                $subBlockLabels =   $subBlockClass->attributeLabels();
                                ?>
                                <div class="block">
                                    <h2>Блок: <?=$block->attributes['title'];?></h2>
                                    <div class="block_btn">
                                        <?=Html::a(
                                            'Редактировать',
                                            ['admin/edit/block/' . $block->attributes['id']],
                                            ['class' => 'btn btn-primary']
                                        );?>
                                        <?=Html::a(
                                            'Добавить подблок',
                                            ['admin/create/sub_block/'],
                                            ['class' => 'btn btn-primary']
                                        );?>
                                    </div>
                                    <?php foreach ($subBlocks as $k => $subBlock): ?>
                                        <?php if ($subBlock->attributes['blockId'] == $block->attributes['id']): ?>
                                            <div class="block_sup">
                                                <h3><?=$subBlock->attributes['title'];?></h3>
                                                <?=Html::a(
                                                    'Редактировать',
                                                    ['admin/edit/sub_block/' . $subBlock->attributes['id']],
                                                    ['class' => 'btn btn-primary']
                                                );?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php else: ?>
                                <div>Подблоков нет</div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div>Блоков нет</div>
                    <?php endif; ?>
                    <?php if (!is_null($blockCommon)): ?>
                        <?php foreach ($blockCommon as $block): ?>
                            <?php
                            $subBlocks = SubBlock::find()->where(['blockId' => $block->attributes['id']])->all();
                            ?>
                            <?php if (!is_null($subBlocks)): ?>
                                <?php
                                $subBlockClass  =   new SubBlock();
                                $subBlockLabels =   $subBlockClass->attributeLabels();
                                ?>
                                <div class="block">
                                    <h2>Блок: <?=$block->attributes['title'];?> (Общий)</h2>
                                    <div>
                                        <?=Html::a(
                                            'Редактировать',
                                            ['admin/edit/block/' . $block->attributes['id']],
                                            ['class' => 'btn btn-primary']
                                        );?>
                                        <?=Html::a(
                                            'Добавить подблок',
                                            ['admin/create/sub_block/'],
                                            ['class' => 'btn btn-primary']
                                        );?>
                                    </div>
                                    <?php foreach ($subBlocks as $k => $subBlock): ?>
                                        <?php if ($subBlock->attributes['blockId'] == $block->attributes['id']): ?>
                                            <div class="block_sup">
                                                <h3><?=$subBlock->attributes['title'];?></h3>
                                                <?=Html::a(
                                                    'Редактировать',
                                                    ['admin/edit/sub_block/' . $subBlock->attributes['id']],
                                                    ['class' => 'btn btn-primary']
                                                );?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php else: ?>
                                <div>Подблоков нет</div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div>Блоков нет</div>
                    <?php endif; ?>
                </div>
                <div id="news" class="tab-pane fade">
                    <h1><?=$newsPage->attributes['title'];?></h1>
                    <?=Html::a(
                            'Редактировать',
                            ['admin/edit/page/' . $newsPage->attributes['id']],
                            ['class' => 'btn btn-primary']
                    );?>
                    <div class="block">
                        <h4>Мета информация</h4>
                        <?php
                        $metas = Meta::find()->where([
                            'typeId'    =>  $mainPage->attributes['id'],
                            'type'      =>  'page'
                        ])->all();
                        ?>
                        <?php if (!is_null($metas) && !empty($metas)): ?>
                            <?php
                            $metaClass  =   new Meta();
                            $metaLabels =   $metaClass->attributeLabels();
                            ?>
                            <div class="block_btn">
                                <?=Html::a(
                                    'Добавить мета тег',
                                    ['admin/create/meta/'],
                                    ['class' => 'btn btn-primary']
                                );?>
                            </div>
                            <?php foreach ($metas as $meta):?>
                                <div class="item_meta">
                                    <h4>Тип: <?=$meta->attributes['name'];?></h4>
                                    <p>Содержание: <?=$meta->attributes['value'];?></p>
                                    <?=Html::a('Редактировать', ['/admin/edit/meta/' . $meta->attributes['id']]);?>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="item_meta">
                                <p>Мета тегов нет</p>
                                <?=Html::a(
                                    'Добавить мета тег',
                                    ['admin/create/meta/'],
                                    ['class' => 'btn btn-primary']
                                );?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <h2>Список новостей:</h2>
                    <?php if (!is_null($posts)):
                        arsort($posts); ?>
                    <div class="block_btn">
                        <?=Html::a(
                            'Добавить новость',
                            ['admin/create/post/'],
                            ['class' => 'btn btn-primary']
                        );?>
                    </div>
                        <?php foreach ($posts as $post): ?>
                            <div class="item item_post">
                                <h2>Новость: <?=$post->attributes['title'];?></h2>
                                <div class="block_btn">
                                    <?=Html::a('Редактировать', ['/admin/edit/post/' . $post->attributes['id']],['class' => 'btn btn-primary']);?>
                                </div>
                                <p>Дата создания: <?=Yii::$app->formatter->asDatetime($post->attributes['created']);?></p>
                                <p>Дата последнего изменения: <?=Yii::$app->formatter->asDatetime($post->attributes['updated']);?></p>
                                <p>Краткое описание:</p>
                                <p><?=$post->attributes['summary'];?></p>
                                <p>Статус: <?=$status[$post->attributes['status']];?></p>
                                <h4>Мета информация</h4>
                                <?php
                                $metas = Meta::find()->where([
                                    'typeId'    =>  $post->attributes['id'],
                                    'type'      =>  'post'
                                ])->all();
                                ?>
                                <?php if (!is_null($metas) && !empty($metas)): ?>
                                    <?php
                                    $metaClass  =   new Meta();
                                    $metaLabels =   $metaClass->attributeLabels();
                                    ?>
                                    <div class="block_btn">
                                        <?=Html::a(
                                            'Добавить мета тег',
                                            ['admin/create/meta/'],
                                            ['class' => 'btn btn-primary']
                                        );?>
                                    </div>
                                    <?php foreach ($metas as $meta):?>
                                        <div class="item_meta">
                                            <h4>Тип: <?=$meta->attributes['name'];?></h4>
                                            <p>Содержание: <?=$meta->attributes['value'];?></p>
                                            <?=Html::a('Редактировать', ['/admin/edit/meta/' . $meta->attributes['id']]);?>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <div class="item_meta">
                                        <p>Мета тегов нет</p>
                                        <?=Html::a(
                                            'Добавить мета тег',
                                            ['admin/create/meta/'],
                                            ['class' => 'btn btn-primary']
                                        );?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div>Новостей нет</div>
                        <?=Html::a(
                            'Добавить новость',
                            ['admin/create/post/'],
                            ['class' => 'btn btn-primary']
                        );?>
                    <?php endif; ?>
                </div>
                <div id="gallery" class="tab-pane fade">
                    <?php if (!is_null($albums) && !empty($albums)): ?>
                    <div class="tab-content panel-body">
                        <div id="blocks_gallery">
                            <h1>Фотоотчеты</h1>
                            <?=Html::a(
                                'Добавить альбом',
                                ['admin/create/albums/'],
                                ['class' => 'btn btn-primary']
                            );?>
                            <?php foreach ($albums as $album): ?>
                                <div class="block">
                                    <h2>Название альбома: <?=$album->attributes['title'];?></h2>
                                    <?=Html::a(
                                        'Редактировать',
                                        ['admin/edit/albums/' . $album->attributes['id']],
                                        ['class' => 'btn btn-primary']
                                    );?>
                                    <div>Описание альбома: <?=$album->attributes['description'];?></div>
                                    <div>Статус альбома: <?=$status[$album->attributes['status']];?></div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?php else: ?>
                            <div class="col-lg-12 text-center">
                                <h1 class="main">Альбомов нет</h1>
                                <?=Html::a(
                                    'Добавить альбом',
                                    ['admin/create/albums/'],
                                    ['class' => 'btn btn-primary']
                                );?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div id="pages" class="tab-pane fade">
                    <div class="form-group text-center">
                        <?=Html::a(
                            'Добавить',
                            ['admin/create/page/'],
                            ['class' => 'btn btn-primary']
                        );?>
                    </div>
                    <h2>Список страниц</h2>
                    <?php if (!is_null($pages) && !empty($pages)): ?>
                        <ul class="nav nav-pills panel-heading">
                            <li class="active"><a data-toggle="tab" href="#mobilPages">Страницы Мобил</a></li>
                            <li><a data-toggle="tab" href="#regataPages">Страницы Регата</a></li>
                        </ul>
                        <div class="tab-content panel-body">
                            <div id="mobilPages" class="tab-pane fade in active">
                                <?php foreach ($pages as $page):?>
                                    <?php if(substr( $page->attributes['name'], 0, 6 ) != "regata"): ?>
                                        <div class="item_pages">
                                            <h2><?=$page->attributes['title'];?></h2>
                                            <p>Статус: <?=$status[$page->attributes['status']];?></p>
                                            <?=Html::a('Редактировать', ['/admin/edit/page/' . $page->attributes['id']], ['class' => 'btn btn-primary']);?>
                                            <h4>Мета информация</h4>
                                            <?php
                                            $metas = Meta::find()->where([
                                                'typeId'    =>  $page->attributes['id'],
                                                'type'      =>  'page'
                                            ])->all();
                                            ?>
                                            <?php if (!is_null($metas) && !empty($metas)): ?>
                                                <?php
                                                $metaClass  =   new Meta();
                                                $metaLabels =   $metaClass->attributeLabels();
                                                ?>
                                                <div class="block_btn">
                                                    <?=Html::a(
                                                        'Добавить мета тег',
                                                        ['admin/create/meta/'],
                                                        ['class' => 'btn btn-primary']
                                                    );?>
                                                </div>
                                                <?php foreach ($metas as $meta):?>
                                                    <div class="item_meta">
                                                        <h4>Тип: <?=$meta->attributes['name'];?></h4>
                                                        <p>Содержание: <?=$meta->attributes['value'];?></p>
                                                        <?=Html::a('Редактировать', ['/admin/edit/meta/' . $meta->attributes['id']]);?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <div class="item_meta">
                                                    <p>Мета тегов нет</p>
                                                    <?=Html::a(
                                                        'Добавить мета тег',
                                                        ['admin/create/meta/'],
                                                        ['class' => 'btn btn-primary']
                                                    );?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif;?>
                                <?php endforeach; ?>
                            </div>
                            <div id="regataPages" class="tab-pane fade in">
                                <?php foreach ($pages as $page):?>
                                    <?php if(substr( $page->attributes['name'], 0, 6 ) === "regata"): ?>
                                        <div class="item_pages">
                                            <h2><?=$page->attributes['title'];?></h2>
                                            <p>Статус: <?=$status[$page->attributes['status']];?></p>
                                            <?=Html::a('Редактировать', ['/admin/edit/page/' . $page->attributes['id']], ['class' => 'btn btn-primary']);?>
                                            <h4>Мета информация</h4>
                                            <?php
                                            $metas = Meta::find()->where([
                                                'typeId'    =>  $page->attributes['id'],
                                                'type'      =>  'page'
                                            ])->all();
                                            ?>
                                            <?php if (!is_null($metas) && !empty($metas)): ?>
                                                <?php
                                                $metaClass  =   new Meta();
                                                $metaLabels =   $metaClass->attributeLabels();
                                                ?>
                                                <div class="block_btn">
                                                    <?=Html::a(
                                                        'Добавить мета тег',
                                                        ['admin/create/meta/'],
                                                        ['class' => 'btn btn-primary']
                                                    );?>
                                                </div>
                                                <?php foreach ($metas as $meta):?>
                                                    <div class="item_meta">
                                                        <h4>Тип: <?=$meta->attributes['name'];?></h4>
                                                        <p>Содержание: <?=$meta->attributes['value'];?></p>
                                                        <?=Html::a('Редактировать', ['/admin/edit/meta/' . $meta->attributes['id']]);?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <div class="item_meta">
                                                    <p>Мета тегов нет</p>
                                                    <?=Html::a(
                                                        'Добавить мета тег',
                                                        ['admin/create/meta/'],
                                                        ['class' => 'btn btn-primary']
                                                    );?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif;?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <div>Страниц нет</div>
                    <?php endif; ?>
                </div>
                <div id="settings" class="tab-pane fade">
                    <div class="form-group text-center">
                        <?=Html::a(
                                'Добавить',
                                ['admin/create/settings/'],
                                ['class' => 'btn btn-primary']
                        );?>
                    </div>
                    <?php if (!is_null($settings) && !empty($settings)): ?>
                        <div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><?=$settingsLabels['name'];?></th>
                                        <th><?=$settingsLabels['value'];?></th>
                                        <th>Редактировать</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($settings as $setting): ?>
                                        <tr>
                                            <th><?=$setting->attributes['name'];?></th>
                                            <th><?=$setting->attributes['value'];?></th>
                                            <th><?=Html::a('Редактировать', ['/admin/edit/settings/' . $setting->attributes['id']]);?></th>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php else: ?>
                        <div class="col-lg-12 text-center">
                            <h1 class="main">Настроек нет</h1>
                        </div>
                    <?php endif; ?>
                </div>
                <div id="feedback" class="tab-pane fade">
                    <?php if (!is_null($feedbacks) && !empty($feedbacks)): ?>
                    <div class="tab-content panel-body">
                        <ul class="nav nav-pills panel-heading">
                            <li class="active"><a data-toggle="tab" href="#feedbackMobilbc">Мобил</a></li>
                            <li><a data-toggle="tab" href="#feedbackRegata">Регата</a></li>
                            <li><a data-toggle="tab" href="#feedbackPremises">Помещения</a></li>
                        </ul>
                        <div id="feedbackMobilbc" class="tab-pane fade in active">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="row"><?=$feedbackLabels['id'];?></th>
                                        <th><?=$feedbackLabels['to'];?></th>
                                        <th><?=$feedbackLabels['from'];?></th>
                                        <th><?=$feedbackLabels['name'];?></th>
                                        <th><?=$feedbackLabels['phone'];?></th>
                                        <th><?=$feedbackLabels['subject'];?></th>
                                        <th><?=$feedbackLabels['body'];?></th>
                                        <th><?=$feedbackLabels['created'];?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($feedbacks as $feedback):
                                        if($feedback->attributes['subject'] == 'Заявка с сайта БЦ Мобил'):
                                            ?>
                                        <tr>
                                            <th><?=$feedback->attributes['id']?></th>
                                            <th><?=$feedback->attributes['to'];?></th>
                                            <th><?=$feedback->attributes['from'];?></th>
                                            <th><?=$feedback->attributes['name'];?></th>
                                            <th><?=$feedback->attributes['phone'];?></th>
                                            <th><?=$feedback->attributes['subject'];?></th>
                                            <th><?=$feedback->attributes['body'];?></th>
                                            <th><?=Yii::$app->formatter->asDatetime($feedback->attributes['created']);?></th>
                                        </tr>
                                    <?php
                                        endif;
                                    endforeach;
                                    ?>
                                </tbody>
                        </table>
                        </div>
                        <div id="feedbackRegata" class="tab-pane fade">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="row"><?=$feedbackLabels['id'];?></th>
                                    <th><?=$feedbackLabels['to'];?></th>
                                    <th><?=$feedbackLabels['from'];?></th>
                                    <th><?=$feedbackLabels['name'];?></th>
                                    <th><?=$feedbackLabels['phone'];?></th>
                                    <th><?=$feedbackLabels['subject'];?></th>
                                    <th><?=$feedbackLabels['body'];?></th>
                                    <th><?=$feedbackLabels['created'];?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($feedbacks as $feedback):
                                    if($feedback->attributes['subject'] == 'Заявка с сайта Регата Мобил'):
                                        ?>
                                        <tr>
                                            <th><?=$feedback->attributes['id']?></th>
                                            <th><?=$feedback->attributes['to'];?></th>
                                            <th><?=$feedback->attributes['from'];?></th>
                                            <th><?=$feedback->attributes['name'];?></th>
                                            <th><?=$feedback->attributes['phone'];?></th>
                                            <th><?=$feedback->attributes['subject'];?></th>
                                            <th><?=$feedback->attributes['body'];?></th>
                                            <th><?=Yii::$app->formatter->asDatetime($feedback->attributes['created']);?></th>
                                        </tr>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="feedbackPremises" class="tab-pane fade">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="row"><?=$feedbackLabels['id'];?></th>
                                    <th><?=$feedbackLabels['to'];?></th>
                                    <th><?=$feedbackLabels['from'];?></th>
                                    <th><?=$feedbackLabels['name'];?></th>
                                    <th><?=$feedbackLabels['phone'];?></th>
                                    <th><?=$feedbackLabels['subject'];?></th>
                                    <th><?=$feedbackLabels['body'];?></th>
                                    <th><?=$feedbackLabels['created'];?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($feedbacks as $feedback):
                                    if($feedback->attributes['subject'] == 'Заявка помещения'):
                                        ?>
                                        <tr>
                                            <th><?=$feedback->attributes['id']?></th>
                                            <th><?=$feedback->attributes['to'];?></th>
                                            <th><?=$feedback->attributes['from'];?></th>
                                            <th><?=$feedback->attributes['name'];?></th>
                                            <th><?=$feedback->attributes['phone'];?></th>
                                            <th><?=$feedback->attributes['subject'];?></th>
                                            <th><?=$feedback->attributes['body'];?></th>
                                            <th><?=Yii::$app->formatter->asDatetime($feedback->attributes['created']);?></th>
                                        </tr>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php else: ?>
                        <div class="col-lg-12 text-center">
                            <h1 class="main">Писем нет</h1>
                        </div>
                    <?php endif; ?>
                </div>
                <div id="objects" class="tab-pane fade">
                    <div class="form-group text-center">
                        <?=Html::a(
                            'Создать',
                            ['admin/create/object/'],
                            ['class' => 'btn btn-primary']
                        );?>
                    </div>
                    <?php if (!is_null($objects) && !empty($objects)): ?>
                        <div class="tab-content panel-body">
                            <ul class="nav nav-pills panel-heading">
                                <li class="active"><a data-toggle="tab" href="#floor_1">Первый этаж</a></li>
                                <li><a data-toggle="tab" href="#floor_2">Второй этаж</a></li>
                                <li><a data-toggle="tab" href="#floor_3">Третий этаж</a></li>
                            </ul>
                            <div id="floor_1" class="tab-pane fade in active">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="row"><?=$objectLabels['id'];?></th>
                                        <th><?=$objectLabels['square'];?></th>
                                        <th><?=$objectLabels['floor'];?></th>
                                        <th><?=$objectLabels['title'];?></th>
                                        <th><?=$objectLabels['status'];?></th>
                                        <th>Редактировать</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($objects as $object):
                                        if($object->attributes['floor'] == '1'): ?>
                                            <tr>
                                                <th><?=$object->attributes['id'];?></th>
                                                <th><?=$object->attributes['square'];?></th>
                                                <th><?=$object->attributes['floor'];?></th>
                                                <th><?=$object->attributes['title'];?></th>
                                                <th><?=$status[$object->attributes['status']];?></th>
                                                <th><?=Html::a('Редактировать', ['/admin/edit/object/' . $object->attributes['id']]);?></th>
                                            </tr>
                                    <?php
                                        endif;
                                    endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="floor_2" class="tab-pane fade">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="row"><?=$objectLabels['id'];?></th>
                                        <th><?=$objectLabels['square'];?></th>
                                        <th><?=$objectLabels['floor'];?></th>
                                        <th><?=$objectLabels['title'];?></th>
                                        <th><?=$objectLabels['status'];?></th>
                                        <th>Редактировать</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($objects as $object):
                                        if($object->attributes['floor'] == '2'): ?>
                                        <tr>
                                            <th><?=$object->attributes['id'];?></th>
                                            <th><?=$object->attributes['square'];?></th>
                                            <th><?=$object->attributes['floor'];?></th>
                                            <th><?=$object->attributes['title'];?></th>
                                            <th><?=$status[$object->attributes['status']];?></th>
                                            <th><?=Html::a('Редактировать', ['/admin/edit/object/' . $object->attributes['id']]);?></th>
                                        </tr>
                                    <?php
                                        endif;
                                    endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="floor_3" class="tab-pane fade">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="row"><?=$objectLabels['id'];?></th>
                                        <th><?=$objectLabels['square'];?></th>
                                        <th><?=$objectLabels['floor'];?></th>
                                        <th><?=$objectLabels['title'];?></th>
                                        <th><?=$objectLabels['status'];?></th>
                                        <th>Редактировать</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($objects as $object):
                                        if($object->attributes['floor'] == '3'): ?>
                                        <tr>
                                            <th><?=$object->attributes['id'];?></th>
                                            <th><?=$object->attributes['square'];?></th>
                                            <th><?=$object->attributes['floor'];?></th>
                                            <th><?=$object->attributes['title'];?></th>
                                            <th><?=$status[$object->attributes['status']];?></th>
                                            <th><?=Html::a('Редактировать', ['/admin/edit/object/' . $object->attributes['id']]);?></th>
                                        </tr>
                                    <?php
                                        endif;
                                    endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="col-lg-12 text-center">
                            <h1 class="main">Объектов нет</h1>
                        </div>
                    <?php endif; ?>
                </div>
        </div>
    </div>
</div>
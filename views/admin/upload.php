<?php
    use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
        'action'    =>  '/admin/uploads/' . $albumsId . '/',
        'method'    =>  'post',
        'options'   =>  [
            'enctype'   =>  'multipart/form-data',
            'class'     =>  'form-horizontal',
            'id'        =>  'upload_images'
        ]
    ]);

?>

    <?= $form->field($model, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']); ?>

    <button type="submit" class="btn btn-primary">Upload</button>

    <script>
        function preLoader (on, time, callback) {
            var $preloader = $('#preloader');

            if (on == true) {
                if(typeof callback != 'undefined') {
                    $preloader.css({"display": "block"});
                    setTimeout(function() { $preloader.fadeIn(time, callback); }, time);
                } else {
                    $preloader.css({"display": "block"});
                    $preloader.fadeIn(time);
                }
            } else {
                $preloader.fadeOut(time);
                $preloader.css({"display": "none"});
            }
        }

        $('#upload_images').on('beforeSubmit', function() {

            preLoader(true);

            var formData = new FormData($('#upload_images')[0]);
            console.log(formData);

            $.ajax({
                url     :   $(this).attr("action"),
                type    :   $(this).attr("method"),
                data    :   formData,
                success :   function () {
                    preLoader(false);
                    parent.jQuery.fancybox.close();
                    parent.window.location.reload();
                },
                error   :   function () {
                    preLoader(false);
                    parent.jQuery.fancybox.close();
                    parent.window.location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }).on('submit', function(e){
            e.preventDefault();
        });

    </script>

<?php ActiveForm::end() ?>
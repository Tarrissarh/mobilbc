<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="preloader" style="display:none">
    <img src="/img/loading.png" alt="loading">
</div>
<div class="wrap">
    <div class="container b-logout">
        <?php

        echo Html::a('Перейти на сайт', Yii::$app->homeUrl);

        if (!Yii::$app->user->isGuest) {
            echo Html::beginForm('logout', 'post');
            echo Html::submitButton('Выход', ['class' => 'btn btn-link logout pull-right']);
            echo Html::endForm();
        }

        ?>
    </div>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Mobilbc.ru <?= date('Y'); ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

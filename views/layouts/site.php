<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\SiteAsset;

SiteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1.0">
    <?= Html::csrfMetaTags() ?>
    <?php if (!isset($this->context->attr['albums'])): ?>
        <?php if (!is_null($this->context->attr['metas'])): ?>
            <?php foreach ($this->context->attr['metas'] as $meta): ?>
                <meta name="<?=$meta->attributes['name'];?>" content="<?=$meta->attributes['value'];?>">
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endif; ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?=(stristr($this->context->attr['page']['name'], 'regatta'))?'regata':'';?>" >
<?php $this->beginBody() ?>
    <?php if (stristr($this->context->attr['page']['name'], 'regatta') !== false): ?>
        <nav class="nav">
        <div class="container">
            <ul class="nav__links">
                <?php if (!is_null($this->context->attr['menu'])): ?>
                    <?php foreach ($this->context->attr['menu'] as $menu): ?>
                        <?php if ($menu['url'] === '/'): ?>
                            <li><a class="link nav__link" href="<?=$menu['url'];?>"><?=$menu['title'];?></a></li>
                        <?php else: ?>
                            <li><a class="link nav__link" href="/<?=$menu['url'];?>/"><?=$menu['title'];?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <li><a class="link nav__link" href="/">Бизнес-центр Mobil</a></li>
                <li>
                    <a href="tel:<?=Yii::$app->params['phoneRegata'];?>">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <span><?=Yii::$app->params['phoneRegata'];?></span>
                    </a>
                    <div class="social">
                        <a href="<?=Yii::$app->params['facebook'];?>">
                            <i class="fa fa-facebook-square" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
            </ul>
            <div id="hamburger"><span></span><span></span><span></span></div>
        </div>
    </nav>
    <?php else: ?>
        <nav class="nav">
        <div class="container">
            <ul class="nav__links">
                <?php if (!is_null($this->context->attr['menu'])): ?>
                    <?php foreach ($this->context->attr['menu'] as $menu): ?>
                        <?php if ($menu['url'] === '/'): ?>
                            <li><a class="link nav__link" href="<?=$menu['url'];?>"><?=$menu['title'];?></a></li>
                        <?php else: ?>
                            <li><a class="link nav__link" href="/<?=$menu['url'];?>/"><?=$menu['title'];?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <li><a class="link nav__link" href="http://cardinale.ru" target="_blank">Ресторанный комплекс</a></li>
                <li><a class="link nav__link" href="/regatta/">Регата Mobil</a></li>
                <li>
                    <a href="tel:<?=Yii::$app->params['phone'];?>">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <span><?=Yii::$app->params['phone'];?></span>
                    </a>
                </li>
            </ul>
            <div id="hamburger"><span></span><span></span><span></span></div>
        </div>
    </nav>
    <?php endif; ?>
<main>
    <?= $content ?>
</main>
<footer class="footer">
    <div class="container flex">
        <div class="footer__item">
            <?php if (stristr($this->context->attr['page']['name'], 'regatta') !== false): ?>
                <div class="social">
                    <a href="<?=Yii::$app->params['facebook'];?>">
                        <i class="fa fa-facebook-square" aria-hidden="true"></i>
                    </a>
                </div>
            <?php else: ?>
            <img src="/img/Mobil_logo.svg" class="footer__logo">
            <?php endif; ?>
        </div>
        <div class="footer__item">
            <?php if (stristr($this->context->attr['page']['name'], 'regatta') !== false): ?>
                <span><i class="fa fa-map-marker" aria-hidden="true"></i><?=Yii::$app->params['addressRegata'];?></span>
                <br/>
                <a href="tel:<?=Yii::$app->params['phone'];?>"><i class="fa fa-phone" aria-hidden="true"></i><?=Yii::$app->params['phoneRegata'];?></a>
            <?php else: ?>
                <span><i class="fa fa-map-marker" aria-hidden="true"></i><?=Yii::$app->params['address'];?></span>
                <br/>
                <a href="tel:<?=Yii::$app->params['phone'];?>"><i class="fa fa-phone" aria-hidden="true"></i><?=Yii::$app->params['phone'];?></a>
            <?php endif; ?>
        </div>
        <div class="footer__item">
            <p>© <?=date("Y");?> Mobilbc.ru</p>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

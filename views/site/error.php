<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$this->registerJsFile('@web/js/pages.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile('@web/css/pages.css');
?>
<div class="site-error container page_wrapper">

    <?php if ($exception->statusCode == '404'): ?>
        <?php $this->title = "Страница не найдена"; ?>
        <h2 class="text-danger text-center lead">Такой страницы нет.</h2>
    <?php else: ?>
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="alert alert-danger">
            <?= nl2br(Html::encode($message)) ?>
        </div>
        <h2> Ошибка при подключении. Попробуйте снова. </h2>
    <?php endif; ?>

</div>

<?php

$this->title = $this->context->attr['albums']['title'];
$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css');
$this->registerJsFile('@web/js/pages.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile('@web/css/pages.css');

?>

<div class="container page_wrapper">
    <div class="text-center">
        <a class="btn btn_more btn_more_water" href="/regatta/photoreports/">Назад к альбомам</a>
    </div>
    <div class="photo__block">
        <?php if (!empty($gallery)):
            arsort($gallery);
            $i = 0; ?>
            <?php foreach ($gallery as $item):?>
                <?php if ($i < 20):
                    $i++;?>
                    <a href="/uploads/<?=$item['urlImage'];?>" class="photo__item" data-fancybox="images">
                        <div class="photo__wrapper">
                            <img src="/uploads/<?=$item['urlImage'];?>" alt="<?=$item['alt'];?>" class="fancy-false">
                        </div>
                    </a>
                <?php else: ?>
                    <a href="/uploads/<?=$item['urlImage'];?>" class="photo__item" data-fancybox="images" style="display: none">
                        <div class="photo__wrapper">
                            <img src="/uploads/<?=$item['urlImage'];?>" alt="<?=$item['alt'];?>" class="fancy-false">
                        </div>
                    </a>
                <?php endif; ?>
            <?php endforeach; ?>
            <div class="text-center">
                <div class="btn btn_more btn_more_water js-show_more" data-from="20" data-amount="20" data-max="<?=count($gallery)?>">Показать еще</div>
            </div>
        <?php else: ?>
            <div>Изображений пока нет, зайдите позже.</div>
        <?php endif ?>
    </div>
</div>

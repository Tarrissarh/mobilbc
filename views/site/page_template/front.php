<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = $page['title'];

$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('@web/js/index.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('@web/js/feedback.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css');
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css');
$this->registerCssFile('//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css');
$this->registerCssFile('@web/css/index.css');
?>
<header class="header intro">
    <div class="intro__overlay"></div>
    <video class="intro__video" autoplay loop>
        <source src="http://mobilbc.ru/mobilbc2.webm" type="video/webm">
        <source src="http://mobilbc.ru/mobilbc2.mp4" type="video/mp4">
    </video>
    <div class="intro__text">
        <div class="container">
            <div class="intro__title">
                <h1>Бизнес-центр</h1>
                <div class="mobiltm">
                    <img src="/img/Mobil_logo_color.svg">
                </div>
                <p>на 15 км МКАД</p>
            </div>
            <?php foreach ($blocks as $key => $block): ?>
                <?php if ($block['block']['name'] == 'key_info'): ?>
                    <?php foreach ($block['sub_block'] as $key2 => $sub_block): ?>
                        <div class="intro__info"><?=$sub_block['content']; ?></div>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</header>
<?php foreach ($blocks as $key => $block): ?>
    <?php if ($block['block']['name'] == 'advantages'): ?>
        <section class="advantages">
            <div class="container">
                        <h2><?=$block['block']['title']; ?></h2>
                        <?php foreach ($block['sub_block'] as $key2 => $sub_block): ?>
                            <a class="advantages__item" data-src="#advantages__text_<?=$sub_block['id']; ?>" href="javascript:;">
                                <?php
                                    if (!is_null($sub_block['imageFile']) && !empty($sub_block['imageFile'])) {
                                        $imageUrl = Url::to('@web/uploads/') . $sub_block['imageFile'];
                                    } else {
                                        $imageUrl = Url::to('@web/img/car-wheel.svg');
                                    }
                                ?>
                                <img class="item__img" src="<?=$imageUrl;?>">
                                <div class="item__title"><?=$sub_block['title']; ?></div>
                                <div class="item__text_short"><?=$sub_block['shortContent']; ?></div>
                                <div id="advantages__text_<?=$sub_block['id']; ?>"><?=$sub_block['content']; ?></div>
                            </a>
                        <?php endforeach; ?>
            </div>
            <a class="btn btn_more btn_more_blue" href="/about/">Подробнее</a>
        </section>
    <?php endif; ?>
    <?php if ($block['block']['name'] == 'premises'): ?>
        <section class="premises">
            <div class="container">
                <h2><?=$block['block']['title']; ?></h2>
                <div class="flex">
                    <?php foreach ($block['sub_block'] as $key2 => $sub_block): ?>
                        <div class="column">
                            <div class="premises__title"><?=$sub_block['title']; ?></div>
                            <div class="premises__text"><?=$sub_block['content']; ?></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <a class="btn btn_more btn_more_blue" href="/premises/">Подробнее</a>
        </section>
    <?php endif; ?>
    <?php if ($block['block']['name'] == 'partners'): ?>
        <section class="partners">
            <div class="container">
                <h2><?=$block['block']['title']; ?></h2>
                <div class="partners__block">
                    <?php foreach ($block['sub_block'] as $key2 => $sub_block): ?>
                        <a class="partners__item" data-src="#partners__text_<?=$sub_block['id']; ?>" href="javascript:;"">
                            <?php
                                if (!is_null($sub_block['imageFile']) && !empty($sub_block['imageFile'])) {
                                    $imageUrl = Url::to('@web/uploads/') . $sub_block['imageFile'];
                                } else {
                                    $imageUrl = Url::to('@web/img/avto_start.png');
                                }
                            ?>
                            <img class="item__img" src="<?=$imageUrl;?>">
                        </a>
                        <div id="partners__text_<?=$sub_block['id']?>"><?=$sub_block['content']; ?></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
    <?php if ($block['block']['name'] == 'contacts'): ?>
        <section class="contacts">
            <div class="container">
                <h2><?=$block['block']['title']; ?></h2>
                <?php foreach ($block['sub_block'] as $key2 => $sub_block): ?>
                    <div class="contacts__item">
                        <div class="contacts__item__img"></div>
                        <div class="contacts__item__title"><?=$sub_block['title']; ?></div>
                        <p><?=$sub_block['content']; ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        <a class="btn btn_more btn_more_blue" href="/contacts/">Подробнее</a>
        </section>
    <?php endif; ?>
    <?php if ($block['block']['name'] == 'feedback'): ?>
        <section class="feedback">
            <div class="container flex">
                <?php $form = ActiveForm::begin(['id' => 'contactForm', 'options' => ['class' => 'column']]); ?>

                    <?= $form->field($modelContact, 'name')->textInput(['placeholder' => 'Имя'])->label(false); ?>

                    <?= $form->field($modelContact, 'email')->textInput(['placeholder' => 'E-mail'])->label(false); ?>

                    <?= $form->field($modelContact, 'phone')->textInput([
                        'placeholder' => 'Телефон',
                        'maxlength'     =>  10,
                    ])->label(false); ?>

                    <?= $form->field($modelContact, 'subject')->hiddenInput(['value' => 'Заявка с сайта БЦ Мобил'])->label(false); ?>

                    <?= $form->field($modelContact, 'body')->textarea(['rows' => 6, 'placeholder' => 'Сообщение'])->label(false); ?>

                    <?= $form->field($modelContact, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className())->label(false); ?>

                <?= $form->field($modelContact, 'agreement')->checkbox([
                    'checked'   =>  false,
                    'required'  =>  true,
                    'options' => ['class' => 'agreement']
                ], false)->label(true); ?>

                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn_more btn_more_blue', 'name' => 'contact-button']); ?>
                    </div>

                <?php ActiveForm::end(); ?>
                <div class="column text-left">
                    <h2><?=$block['block']['title']; ?></h2>
                    <?php foreach ($block['sub_block'] as $key2 => $sub_block): ?>
                        <div class="feedback__item"><?=$sub_block['content']; ?></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
<?php endforeach; ?>

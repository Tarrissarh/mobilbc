<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $this->context->attr['page']['title'];
$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css');
$this->registerJsFile('@web/js/pages.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile('@web/css/pages.css');

?>

<div class="container page_wrapper">
    <h1>Альбомы</h1>
    <div class="photo__block">
        <?php foreach ($albums as $album): ?>
            <a href="/regatta/albums/<?=$album->attributes['id'];?>" class="photo__item">
                <div class="photo__wrapper">
                    <img src="#" alt="" id="<?=$album->attributes['id'];?>" class="fancy-false">
                    <h3><?=$album->attributes['title'];?></h3>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        <?php foreach ($albums as $album): ?>
        $.ajax({
            url     :   '/ajax/albums/' + <?=$album->attributes['id'];?> + '/',
            type    :   'post',
            success :   function (photo) {
                var photoArr = $.parseJSON($.parseJSON(photo).image);

                if (photoArr != null) {
                    $('#' + photoArr.albumsId).attr('src', '/uploads/' + photoArr.urlImage).attr('alt', photoArr.alt);
                } else {
                    $('#' + <?=$album->attributes['id'];?>).attr('src', '/img/Mobil_logo_color.svg').attr('alt', 'Заглушка');
                }
            },
            error   :   function (result) {
                console.log(result);
            }
        });
        <?php endforeach; ?>
    });
</script>

<?php

use yii\helpers\Html;
$this->title = $this->context->attr['page']['title'];
$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css');
$this->registerJsFile('@web/js/pages.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile('@web/css/pages.css');

?>
<div class="container page_wrapper">
    <h1><?=$this->context->attr['page']['title'];?></h1>
    <div class="page__content"><?=$this->context->attr['page']['content'];?></div>
</div>
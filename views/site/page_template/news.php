<?php

use yii\helpers\Html;
use app\models\table\Post;
use app\models\table\Alias;

$this->registerJsFile('@web/js/pages.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css');
$this->registerCssFile('@web/css/pages.css');

$posts = Post::find()->where(['status' => 'publish'])->orderBy(['id'=>SORT_DESC])->all();
foreach ($posts as $key => $post) {
    $allPost[$key] = $post['attributes'];
    $alias = Alias::find()->where(['id' => $allPost[$key]['aliasId']])->one();
    $allPost[$key]['url'] = $alias->attributes['url'];
    unset($allPost[$key]['aliasId']);
}

?>

<div class="container page_wrapper">
    <h1><?=$this->context->attr['page']['title'];?></h1>
    <?php
    $i = 0;
    foreach ($allPost as $key => $post): ?>
        <?php if ($i < 5):
            $i++;
        ?>
                <div class="news__item">
                    <div class="news__title"><?= Html::a($post['title'], '/regatta/' . $post['url'], ['class' => 'btn btn-link']) ?></div>
                    <div class="news__date"><?=$post['created'];?></div>
                    <div class="news__text"><?=$post['summary'];?></div>
                </div>
        <?php else: ?>
                <div class="news__item" style="display: none">
                    <div class="news__title"><?= Html::a($post['title'], '/regatta/' . $post['url'], ['class' => 'btn btn-link']) ?></div>
                    <div class="news__date"><?=$post['created'];?></div>
                    <div class="news__text"><?=$post['summary'];?></div>
                </div>
        <?php endif; ?>
    <?php endforeach; ?>
    <div class="text-center">
        <div class="btn btn_more btn_more_water js-show_more" data-from="5" data-amount="5" data-max="<?=count($allPost)?>">Показать еще</div>
    </div>
</div>
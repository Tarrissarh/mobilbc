<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = $page['title'];

$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('@web/js/regata.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('@web/js/feedback.js', ['depends' => 'yii\web\JqueryAsset']);

$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css');
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css');
$this->registerCssFile('//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css');
$this->registerCssFile('@web/css/regata.css');
?>
<header class="header intro">
    <div class="intro__overlay"></div>
    <video class="intro__video" autoplay loop>
        <source src="http://mobilbc.ru/regata_webm.webm " type="video/webm">
        <source src="http://mobilbc.ru/regata_mp4.mp4" type="video/mp4">
    </video>
    <div class="intro__text">
        <div class="container">
            <div class="intro__title">
                <h1>Парусная неделя на&nbsp;кубок бизнес-центра<div class="mobiltm"><img src="/img/Mobil_logo_color.svg"></div></h1>
                <p>с 03.06.17 по 10.06.17</p>
            </div>
            <?php foreach ($blocks as $key => $block): ?>
                <?php if ($block['block']['name'] == 'regatta_key_info'): ?>
                    <?php foreach ($block['sub_block'] as $key2 => $sub_block): ?>
                        <div class="intro__info"><?=$sub_block['content']; ?></div>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</header>
<?php foreach ($blocks as $key => $block): ?>
    <?php if ($block['block']['name'] == 'regatta_about'): ?>
        <section class="about">
            <div class="container">
                <h2><?=$block['block']['title']; ?></h2>
                <?php foreach ($block['sub_block'] as $key2 => $sub_block): ?>
                    <div class="about__item"><?=$sub_block['content']; ?></div>
                <?php endforeach; ?>
            </div>
            <a class="btn btn_more btn_more_water" href="/regatta/event/">Подробнее</a>
        </section>
    <?php endif; ?>
    <?php if ($block['block']['name'] == 'regatta_news'): ?>
        <section class="news">
            <div class="container">
                <h2><?=$block['block']['title']; ?></h2>
                <div class="flex">
                    <?php if (!is_null($posts)): ?>
                        <?php foreach ($posts as $key => $post): ?>
                            <?php if (!is_null($post)): ?>
                                <div class="column">
                                    <div class="wrapper">
                                        <div class="news__title"><?=$post['title']; ?></div>
                                        <div class="news__date"><?=Yii::$app->formatter->asDatetime($post['created']); ?></div>
                                        <div class="news__text"><?=$post['summary']; ?></div>
                                    </div>
                                    <a href="/regatta/<?=$post['url']; ?>" class="news__link">Подробнее...</a>
                                </div>
                            <?php else: ?>
                                <div class="column">
                                    <div class="wrapper">
                                        <div class="news__title">Новостей нет</div>
                                        <div class="news__date">xx.xx.xxxx</div>
                                        <div class="news__text">Новостей нет</div>
                                    </div>
                                    <a href="#" class="news__link">Подробнее...</a>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <a class="btn btn_more btn_more_turquoise" href="/regatta/news/">Все новости</a>
        </section>
    <?php endif; ?>
    <?php if ($block['block']['name'] == 'partners'): ?>
        <section class="partners">
            <div class="container">
                <h2><?=$block['block']['title']; ?></h2>
                <div class="partners__block">
                    <?php foreach ($block['sub_block'] as $key2 => $sub_block): ?>
                        <a class="partners__item" data-src="#partners__text_<?=$sub_block['id']; ?>" href="javascript:;">
                            <?php
                            if (!is_null($sub_block['imageFile']) && !empty($sub_block['imageFile'])) {
                                $imageUrl = Url::to('@web/uploads/') . $sub_block['imageFile'];
                            } else {
                                $imageUrl = Url::to('@web/img/avto_start.png');
                            }
                            ?>
                            <img class="item__img" src="<?=$imageUrl;?>">
                        </a>
                        <div id="partners__text_<?=$sub_block['id']?>"><?=$sub_block['content']; ?></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
    <?php if ($block['block']['name'] == 'regatta_photo' && !is_null($galleryImages)): ?>
        <section class="photo">
            <div class="container">
                <h2><?=$block['block']['title']; ?></h2>
                <div class="photo__block">
                    <?php
                    arsort($galleryImages);
                    $output = array_slice($galleryImages, 0, 8);
                    foreach ($output as $galleryImage): ?>
                        <a href="<?=Url::to('@web/uploads/') . $galleryImage['urlImage'];?>" class="photo__item" data-fancybox="images">
                            <div class="photo__wrapper">
                                <img src="<?=Url::to('@web/uploads/') . $galleryImage['urlImage'];?>" alt="<?=$galleryImage['alt'];?>">
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
            <a class="btn btn_more btn_more_turquoise" href="/regatta/photoreports/">Все фотографии</a>
        </section>
    <?php endif; ?>
    <?php if ($block['block']['name'] == 'regatta_feedback'): ?>
        <section class="feedback">
            <div class="container flex">
                <?php
                    $form = ActiveForm::begin([
                        'id'        =>  'contactForm',
                        'action'    =>  '/sendmail/',
                        'method'    =>  'post',
                        'options'   =>  ['class' => 'column']
                    ]);
                ?>

                    <?= $form->field($modelContact, 'name')->textInput(['placeholder' => 'Имя'])->label(false); ?>

                    <?= $form->field($modelContact, 'email')->textInput(['placeholder' => 'E-mail'])->label(false); ?>

                    <?= $form->field($modelContact, 'phone')->textInput([
                        'placeholder'   =>  'Телефон',
                        'maxlength'     =>  10,
                    ])->label(false); ?>

                    <?= $form->field($modelContact, 'subject')->hiddenInput(['value' => 'Заявка с сайта Регата Мобил'])->label(false); ?>

                    <?= $form->field($modelContact, 'body')->textarea(['rows' => 6, 'placeholder' => 'Сообщение'])->label(false); ?>

                    <?= $form->field($modelContact, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className())->label(false); ?>

                    <?= $form->field($modelContact, 'agreement')->checkbox([
                        'checked'   =>  false,
                        'required'  =>  true,
                        'options' => ['class' => 'agreement']
                    ], false)->label(true); ?>

                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn_more btn_more_water', 'name' => 'contact-button']); ?>
                    </div>

                <?php ActiveForm::end(); ?>

                <div class="column text-left">
                    <h2><?=$block['block']['title']; ?></h2>
                    <?php foreach ($block['sub_block'] as $key2 => $sub_block): ?>
                        <div class="feedback__item"><?=$sub_block['content']; ?></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
<?php endforeach; ?>
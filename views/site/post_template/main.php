<?php

use yii\helpers\Html;
$this->title = $this->context->attr['page']['title'];

$this->title = $this->context->attr['page']['title'];
$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css');
$this->registerJsFile('@web/js/pages.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile('@web/css/pages.css');
?>

<div class="container page_wrapper">
    <div class="news">
        <h1><?=$this->context->attr['page']['title'];?></h1>
        <span class="news__time"><?=$this->context->attr['page']['created'];?></span>
        <div class="page__content"><?=$this->context->attr['page']['content'];?></div>
    </div>
    <div class="text-center">
        <a class="btn btn_more btn_more_water" href="/regatta/news/">Вернуться к списку новостей</a>
    </div>
</div>
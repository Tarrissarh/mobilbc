{
    'use strict';

    function preLoader (on, time, callback) {
        var $preloader = $('#preloader');

        if (on == true) {
            if(typeof callback != 'undefined') {
                $preloader.css({"display": "block"});
                setTimeout(function() { $preloader.fadeIn(time, callback); }, time);
            } else {
                $preloader.css({"display": "block"});
                $preloader.fadeIn(time);
            }
        } else {
            $preloader.fadeOut(time);
            $preloader.css({"display": "none"});
        }
    }

    $(document).ready(function () {
        $('form').on('beforeSubmit', function() {
            preLoader(true);
            $.ajax({
                url     :   $(this).attr("action"),
                type    :   $(this).attr("method"),
                data    :   $(this).serializeArray(),
                success :   function (result) {
                    const data = JSON.parse(result);
                    if (data) {
                        preLoader(false);
                        alert('Ваши изменения сохранены!');
                        location.reload();
                    } else {
                        alert('Ошибка');
                        console.log(data);
                        preLoader(false);
                    }
                },
                error   :   function (result) {
                    alert('Error');
                    console.log(result);
                    preLoader(false);
                }
            });
        }).on('submit', function(e){
            e.preventDefault();
        });

        $('#delete').on('click', function (e) {
            e.preventDefault();
            const answer = confirm('Вы уверены?');
            if (answer) {
                location.href = $(this).attr('href');
            }
        });

        $('.item_gallery .button').on('click', function (e) {
            e.preventDefault();

            if ( $(this).attr('href').indexOf('delete') !== -1 ) {
                var answer = confirm('Вы уверены?');

                if (answer) {
                    $.ajax({
                        url     :   $(this).attr('href'),
                        type    :   'get',
                        success :   function (result) {
                            $('.item_gallery_id_' + result).fadeOut();
                        },
                        error   :   function (result) {
                            alert('Ошибка');
                            console.log(result);
                        }
                    });
                }
            }
        });

        $('#metaType').on('change', function () {
            const type = this.value;
            $.ajax({
                url     :   '/admin/' + type + 's/',
                type    :   'post',
                success :   function (result) {
                    const data      =   JSON.parse(result);
                    const select    =   $('#metaTypeId');
                    var newSelect   =   '';

                    if (type == 'post') {
                        newSelect += '<option value="">Выберите новость</option>';
                    } else if (type == 'page') {
                        newSelect += '<option value="">Выберите страницу</option>';
                    }

                    data.forEach(function(item, i) {
                        newSelect += '<option value="' + item.id + '">' + item.title + '</option>';
                    });

                    select.children().remove();
                    select.append(newSelect)
                },
                error   :   function (result) {
                    alert('Ошибка');
                    console.log(result);
                }
            });
        });
    });
}
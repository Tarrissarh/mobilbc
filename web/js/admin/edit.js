$(document).ready(function () {
    var imageFile = '';
    $('#blocks').on('change', function () {
        if ($(this)[0].value == 2 || $(this)[0].value == 4) {
            $('#imageFile').val(imageFile);
            $('#imageFileBlock').fadeToggle();
        } else {
            imageFile = $('#imageFile').val();
            $('#imageFileBlock').fadeToggle();
            $('#imageFile').val(null);
        }
    });
});
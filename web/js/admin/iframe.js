$(document).ready(function () {
    var iframe = $('.iframe-btn');

    iframe.fancybox({
        type: 'iframe'
    });

    $("[data-fancybox_custom]").on('click', function (e) {
        e.preventDefault();

        $.fancybox.open({
            type        : 'iframe',
            src         : location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '') + $(this).attr('data-src')
        });
    });

    $('#removeUrl').on('click', function () {
        $('#urlImage').val(null);
    });

    $('#removeImageFile').on('click', function () {
        $('#imageFile').val(null);
    });
});
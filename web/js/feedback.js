$(document).ready(function () {
    $('#contactForm').on('beforeSubmit', function() {
        $.ajax({
            url     :   '/sendmail/',
            type    :   $(this).attr("method"),
            data    :   $(this).serializeArray(),
            success :   function (result) {
                alert('Сообщение отправлено!');
            },
            error   :   function (result) {
                alert('Извините, произошла ошибка. Попробуйте позже!');
                console.log(result);
            }
        });
    }).on('submit', function(e){
        e.preventDefault();
    });
});
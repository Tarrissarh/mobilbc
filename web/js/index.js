;function navFixed() {
    var window = $(this),
        st = window.scrollTop(),
        nav = $('.nav');

    if (st > 100) {
        nav.addClass('fixed');
    } else {
        nav.removeClass('fixed');
    }
}



$(window).on('scroll', function () {
    navFixed();
});

$(document).ready(function(){
    navFixed();

    $('#hamburger').on('click', function () {
        $(this).toggleClass('active');
        $('.nav__links').toggleClass('active');

    });

    $('.advantages__item, .partners__item').fancybox();

    $(".partners__block").slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        slide: '.partners__item',
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

});
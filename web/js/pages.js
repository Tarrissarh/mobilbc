$(document).ready(function() {

    $('#hamburger').on('click', function () {
        $(this).toggleClass('active');
        $('.nav__links').toggleClass('active');
    });



    $('.page_wrapper img:not(.fancy-false)').on('click', function (e) {
        e.preventDefault();

        var newArr = [],
            $this = $(this),
            targetIndex = 0;

        $('.page_wrapper img:not(.fancy-false)').each(function (index, el) {
            var $el = $(el),
                src = $el.attr('src');

            if ( src === $this.attr('src') ) {
                targetIndex = index;
            }

            newArr.push({src: src})
        });

        $.fancybox.open(newArr, {
            loop : false
        });

        var $instance = $.fancybox.getInstance();

        $instance.jumpTo( targetIndex, 0 );
    });

    function sliceShowMore(arr, from, to) {
        $.each(arr.slice(from, to), function( i, el ) {
            $(el).fadeIn();
        });
    }

    $('.js-show_more').on('click', function () {
        var $this = $(this),
            from = $this.attr('data-from'),
            to = +from + +$this.attr('data-amount'),
            max = $this.attr('data-max'),
            arr = [];

        if ($('.photo__block').length) {
            arr = $('.photo__block > a');
        } else {
            arr = $('.news__item');
        }

        if (to < max) {
            sliceShowMore(arr, from, to);
            $this.attr('data-from', to);
        } else {
            sliceShowMore(arr, from, to);
            $this.fadeOut();
        }

    });

});
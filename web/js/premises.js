$(document).ready(function(){
    function addClassArrayElem(element, index, array) {
        $(document.getElementsByClassName('room' + element.title)).addClass('active')
    }

    objects.forEach(addClassArrayElem);

    $(document.querySelectorAll('[class^="room"]')).on('click', function () {
        const textId = $(this).attr('class').match(/^[^\s]+/)[0];

        if ($('#premises__text_' + textId).length) {
            $.fancybox.open({
                src  : '#premises__text_' + textId,
                type : 'inline'
            });
        }
    });
});